#!/bin/sh
# Copyright Mattia Bertorello, Nicolò Bidotti 2014
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU  Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU  Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

start=`date +%s`

#step 0: cleanup
hdfs dfs -rm -r -f bitcoin-analysis

#step 1: import blockchain
java -jar blockchain-importer-0.1-SNAPSHOT-full-jar.jar \
	-df bootstrap.dat -n 100000 \
	-ti "hdfs://localhost/user/cloudera/bitcoin-analysis/transactionInputs/ti" \
	-to "hdfs://localhost/user/cloudera/bitcoin-analysis/transactionOutputs/to" \
	-b "hdfs://localhost/user/cloudera/bitcoin-analysis/blocks/b"

#step 2: (drop)create tables from step 1 output
impala-shell -f queries/queries_step_2.sql

#step 3: run transaction transformation pig script
pig transactionTransformation.pig

#step 4: (drop)create tables form step 3 output
impala-shell -f queries/queries_step_4.sql

#step 5: (drop)create table for step 6 output
impala-shell -f queries/queries_step_5.sql

#step 6: run entity discovery computation
hive --service jar giraph-analysis-0.1-SNAPSHOT-job.jar org.apache.giraph.hive.HiveGiraphRunner \
	--computationClass org.unito.bitcoin.giraphAnalysis.EntityDiscoveryComputation \
	-ei "org.apache.giraph.hive.input.edge.examples.HiveIntNullEdge, entities_edges" \
	-o "org.unito.bitcoin.giraphAnalysis.formats.hive.output.HiveOutputIntIntVertex, entities" \
	-hiveconf "mapred.job.tracker=hadoop-master" -hiveconf "giraph.zkList=localhost:2181" \
	-w 1 -hiveconf "giraph.SplitMasterWorker=false"

#step 7: (drop)create parquet version of step 6 output
impala-shell -f queries/queries_step_7.sql
 
#step 8: copy btc to usd exchange rate data and (drop)create table from it
hdfs dfs -mkdir bitcoin-analysis/btcToUsd/
hdfs dfs -put BTC_USD.tsv bitcoin-analysis/btcToUsd/data
impala-shell -f queries/queries_step_8.sql

#step 9: run delete multigraph pig script
pig deleteMultiGraph.pig

#step 10: (drop)create tables from step 8 output
impala-shell -f queries/queries_step_10.sql

#step 11: (drop)create tables for steps 11,12 & 13 (analysis computations)
impala-shell -f queries/queries_step_11.sql

#step 12: run degree distribution computation
hive --service jar giraph-analysis-0.1-SNAPSHOT-job.jar org.apache.giraph.hive.HiveGiraphRunner \
	-hiveconf "giraph.masterComputeClass=org.unito.bitcoin.giraphAnalysis.DegreeDistributionComputation" \
	--computationClass org.unito.bitcoin.giraphAnalysis.DegreeDistributionComputation\$SendOne \
	-ei "org.apache.giraph.hive.input.edge.examples.HiveIntNullEdge, complete_graph" \
	-o "org.unito.bitcoin.giraphAnalysis.formats.hive.output.HiveOutputDegreeDistributionVertex, entities_degree" \
	-hiveconf "mapred.job.tracker=hadoop-master" -hiveconf "giraph.zkList=localhost:2181" \
	-w 1 -hiveconf "giraph.SplitMasterWorker=false"

#step 13: run page rank computation
hive --service jar giraph-analysis-0.1-SNAPSHOT-job.jar org.apache.giraph.hive.HiveGiraphRunner \
	-hiveconf "giraph.masterComputeClass=org.unito.bitcoin.giraphAnalysis.PageRankMasterCompute" \
	--computationClass org.unito.bitcoin.giraphAnalysis.PageRankComputation \
	-ei "org.apache.giraph.hive.input.edge.examples.HiveIntNullEdge, complete_graph" \
	-o "org.unito.bitcoin.giraphAnalysis.formats.hive.output.HiveOutputIntDoubleVertex, entities_page_rank" \
	-hiveconf "mapred.job.tracker=hadoop-master" -hiveconf "giraph.zkList=localhost:2181" \
	-w 1 -hiveconf "giraph.SplitMasterWorker=false"

end=`date +%s`
runtime=$((end-start))
echo ${runtime}