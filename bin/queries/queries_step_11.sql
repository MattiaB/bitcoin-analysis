-- Copyright Mattia Bertorello, Nicolò Bidotti 2014

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU  Lesser General Public License as published
-- by the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.

-- You should have received a copy of the GNU  Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

DROP TABLE IF EXISTS entities_degree;
CREATE EXTERNAL TABLE entities_degree
(entity_id INT, in_degree INT, out_degree INT,
in_neighbourhood_in_degree INT, in_neighbourhood_out_degree INT,
out_neighbourhood_in_degree INT, out_neighbourhood_out_degree INT)
STORED AS rcfile LOCATION '/user/cloudera/bitcoin-analysis/entitiesDegree';

DROP TABLE IF EXISTS entities_page_rank;
CREATE EXTERNAL TABLE entities_page_rank
(entity_id INT, rank DOUBLE)
STORED AS rcfile LOCATION '/user/cloudera/bitcoin-analysis/entitiesPageRank';