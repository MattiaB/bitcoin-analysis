-- Copyright Mattia Bertorello, Nicolò Bidotti 2014

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU  Lesser General Public License as published
-- by the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.

-- You should have received a copy of the GNU  Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

DROP TABLE IF EXISTS transaction_inputs;
CREATE EXTERNAL TABLE transaction_inputs
(block_height BIGINT, transaction_hash STRING, transaction_id BIGINT,
 time BIGINT, input_transaction_hash STRING, output_index BIGINT)
STORED AS parquet
LOCATION '/user/cloudera/bitcoin-analysis/transactionInputs/';

DROP TABLE IF EXISTS transaction_outputs;
CREATE EXTERNAL TABLE transaction_outputs
(block_height BIGINT, transaction_hash STRING, transaction_id BIGINT,
 time BIGINT, address_hash STRING, output_index BIGINT, satoshi BIGINT)
STORED AS parquet
LOCATION '/user/cloudera/bitcoin-analysis/transactionOutputs/';

DROP TABLE IF EXISTS blocks;
CREATE EXTERNAL TABLE blocks
(block_hash STRING, prevBlockHash STRING,
 merkleRoot STRING, height BIGINT, time BIGINT,
 difficulty BIGINT, work BIGINT)
STORED AS parquet
LOCATION '/user/cloudera/bitcoin-analysis/blocks/';