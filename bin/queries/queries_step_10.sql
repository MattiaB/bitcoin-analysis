-- Copyright Mattia Bertorello, Nicolò Bidotti 2014

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU  Lesser General Public License as published
-- by the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.

-- You should have received a copy of the GNU  Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

DROP TABLE IF EXISTS complete_graph;
CREATE EXTERNAL TABLE complete_graph
(from_entity_id INT, to_entity_id INT, dollars DOUBLE, satoshis BIGINT)
STORED AS parquet
LOCATION '/user/cloudera/bitcoin-analysis/completeGraph';

DROP TABLE IF EXISTS transaction_edges_entities_dollars;
CREATE EXTERNAL TABLE transaction_edges_entities_dollars
(block_height BIGINT, transaction_id INT, from_entity_id INT, to_entity_id INT, time BIGINT, 
output_index INT, satoshis BIGINT, dollars DOUBLE)
STORED AS parquet
LOCATION '/user/cloudera/bitcoin-analysis/transactionEdgesEntitiesDollars';