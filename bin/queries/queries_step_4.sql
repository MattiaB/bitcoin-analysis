-- Copyright Mattia Bertorello, Nicolò Bidotti 2014

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU  Lesser General Public License as published
-- by the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.

-- You should have received a copy of the GNU  Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

DROP TABLE IF EXISTS complete_transaction_inputs;
CREATE EXTERNAL TABLE complete_transaction_inputs
(transaction_hash STRING, transaction_id INT, block_time BIGINT,
input_transaction_hash STRING, input_transaction_id INT, output_index INT,
address_hash STRING, address_id INT, satoshis BIGINT)
STORED AS parquet
LOCATION '/user/cloudera/bitcoin-analysis/completeTransactionInputs';

DROP TABLE IF EXISTS complete_transaction_outputs;
CREATE EXTERNAL TABLE complete_transaction_outputs
(transaction_hash STRING, transaction_id INT, block_time BIGINT,
address_hash STRING, address_id INT, output_index INT, satoshis BIGINT)
STORED AS parquet
LOCATION '/user/cloudera/bitcoin-analysis/completeTransactionOutputs';

DROP TABLE IF EXISTS entities_edges;
CREATE EXTERNAL TABLE entities_edges
(from_address_id INT, to_address_id INT)
STORED AS parquet
LOCATION '/user/cloudera/bitcoin-analysis/entitiesEdges';

DROP TABLE IF EXISTS transaction_edges;
CREATE EXTERNAL TABLE transaction_edges
(transaction_id INT, time BIGINT, from_address_id INT, to_address_id INT,
satoshis BIGINT)
STORED AS parquet
LOCATION '/user/cloudera/bitcoin-analysis/transactionEdges';

DROP TABLE IF EXISTS addresses;
CREATE EXTERNAL TABLE addresses
(id INT, hash STRING)
STORED as parquet
LOCATION '/user/cloudera/bitcoin-analysis/addresses';