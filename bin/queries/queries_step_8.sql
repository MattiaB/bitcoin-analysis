-- Copyright Mattia Bertorello, Nicolò Bidotti 2014

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU  Lesser General Public License as published
-- by the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.

-- You should have received a copy of the GNU  Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

DROP TABLE IF EXISTS btc_to_usd;
CREATE EXTERNAL TABLE btc_to_usd(day bigint, exchange_rate double)
   ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\n'
   STORED AS TEXTFILE LOCATION '/user/cloudera/bitcoin-analysis/btcToUsd';