#!/bin/bash
# Copyright Mattia Bertorello, Nicolò Bidotti 2014
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU  Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU  Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

wget -O BTC_USD.csv "http://www.quandl.com/api/v1/datasets/BAVERAGE/USD.csv?trim_start=2010-07-17&trim_end=2014-09-10"
sed '1d' BTC_USD.csv | sed 's/,/\t/g' > BTC_USD.tsv
rm -f BTC_USD.csv