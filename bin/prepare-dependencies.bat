@rem get, compile and install hiveio

git clone git@bitbucket.org:MattiaB/hive-io-experimental.git

cd hive-io-experimental
git checkout porting/hadoop-2.30-cdh5.1.0

call mvn package -pl hive-io-exp-mapreduce,hive-io-exp-core,hive-io-exp-testing -DskipTests -Dmaven.javadoc.skip=true

call mvn install:install-file -Dfile=pom.xml -DgroupId=com.facebook.hiveio -DartifactId=hive-io-exp-parent -Dversion=0.27-SNAPSHOT -Dpackaging=pom -DpomFile=pom.xml -DgeneratePom=true

cd hive-io-exp-core\target
call mvn install:install-file -Dfile=hive-io-exp-core-0.27-SNAPSHOT.jar -DgroupId=com.facebook.hiveio -DartifactId=hive-io-exp-core -Dversion=0.27-SNAPSHOT -Dpackaging=jar -DpomFile=..\pom.xml  -DgeneratePom=true
call mvn install:install-file -Dfile=hive-io-exp-core-0.27-SNAPSHOT-tests.jar -DgroupId=com.facebook.hiveio -DartifactId=hive-io-exp-core -Dversion=0.27-SNAPSHOT -Dpackaging=jar -Dclassifier=tests -DpomFile=..\pom.xml  -DgeneratePom=true

cd ..\..\hive-io-exp-testing\target
call mvn install:install-file -Dfile=hive-io-exp-testing-0.27-SNAPSHOT.jar -DgroupId=com.facebook.hiveio -DartifactId=hive-io-exp-testing -Dversion=0.27-SNAPSHOT -Dpackaging=jar -DpomFile=..\pom.xml  -DgeneratePom=true

cd ..\..\..

@rem get, compile and install giraph

git clone  git@bitbucket.org:m1lt0n/giraph.git

cd giraph
git checkout custom-maven-profile

call mvn package -Pbitcoin-analysis

call mvn install:install-file -Dfile=pom.xml -DgroupId=org.apache.giraph -DartifactId=giraph-parent -Dversion=1.1.0-SNAPSHOT -Dpackaging=pom -DpomFile=pom.xml -DgeneratePom=true

cd giraph-core\target\munged
call mvn install:install-file -Dfile=giraph-1.1.0-SNAPSHOT-for-hadoop-2.3.0-cdh5.1.0.jar -DgroupId=org.apache.giraph -DartifactId=giraph-core -Dversion=1.1.0-SNAPSHOT -Dpackaging=jar -DpomFile=..\..\pom.xml -DgeneratePom=true
call mvn install:install-file -Dfile=giraph-1.1.0-SNAPSHOT-for-hadoop-2.3.0-cdh5.1.0-sources.jar -DgroupId=org.apache.giraph -DartifactId=giraph-core -Dversion=1.1.0-SNAPSHOT -Dpackaging=jar -Dclassifier=src -DpomFile=..\..\pom.xml  -DgeneratePom=true
call mvn install:install-file -Dfile=giraph-1.1.0-SNAPSHOT-for-hadoop-2.3.0-cdh5.1.0-tests.jar -DgroupId=org.apache.giraph -DartifactId=giraph-core -Dversion=1.1.0-SNAPSHOT -Dpackaging=jar -Dclassifier=tests -DpomFile=..\..\pom.xml -DgeneratePom=true

cd ..\..\..\giraph-examples\target\munged
call mvn install:install-file -Dfile=giraph-examples-1.1.0-SNAPSHOT-for-hadoop-2.3.0-cdh5.1.0.jar -DgroupId=org.apache.giraph -DartifactId=giraph-examples -Dversion=1.1.0-SNAPSHOT -Dpackaging=jar -DpomFile=..\..\pom.xml -DgeneratePom=true
call mvn install:install-file -Dfile=giraph-examples-1.1.0-SNAPSHOT-for-hadoop-2.3.0-cdh5.1.0-sources.jar -DgroupId=org.apache.giraph -DartifactId=giraph-examples -Dversion=1.1.0-SNAPSHOT -Dpackaging=jar -Dclassifier=src -DpomFile=..\..\pom.xml -DgeneratePom=true

cd ..\..\..\giraph-hive\target\
call mvn install:install-file -Dfile=giraph-hive-1.1.0-SNAPSHOT.jar -DgroupId=org.apache.giraph -DartifactId=giraph-hive -Dversion=1.1.0-SNAPSHOT -Dpackaging=jar -DpomFile=..\pom.xml -DgeneratePom=true

cd ..\..\..

@rem get, compile and install okapi

git clone https://github.com/grafos-ml/okapi.git

cd okapi
git checkout bb94586b1004808560b19df180ae334fce26ccf2

call mvn package -Phadoop_yarn -Dhadoop.version=2.3.0-cdh5.1.0 -DskipTests

call mvn install:install-file -Dfile=pom.xml -DgroupId=ml.grafos.okapi -DartifactId=okapi -Dversion=0.3.5-SNAPSHOT -Dpackaging=pom -DgeneratePom=true

cd target
call mvn install:install-file -Dfile=okapi-0.3.5-SNAPSHOT.jar -DgroupId=ml.grafos.okapi -DartifactId=okapi -Dversion=0.3.5-SNAPSHOT -Dpackaging=jar -DpomFile=..\pom.xml -DgeneratePom=true

cd ..\..