#!/bin/sh
# Copyright Mattia Bertorello, Nicolò Bidotti 2014
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU  Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU  Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#get, compile and install hiveio
HADOOP_VERSION=2.3.0-cdh5.1.0
HIVE_VERSION=0.12.0-cdh5.1.0
GIRAPH=giraph-1.1.0-SNAPSHOT-for-hadoop-${HADOOP_VERSION}
GIRAPH_EXAMPLES=giraph-examples-1.1.0-SNAPSHOT-for-hadoop-${HADOOP_VERSION}
git clone git@bitbucket.org:MattiaB/hive-io-experimental.git

cd hive-io-experimental
git checkout porting/hadoop-2.30-cdh5.1.0

mvn package -pl hive-io-exp-mapreduce,hive-io-exp-core,hive-io-exp-testing -DskipTests -Dmaven.javadoc.skip=true

mvn install:install-file -Dfile=pom.xml -DgroupId=com.facebook.hiveio -DartifactId=hive-io-exp-parent -Dversion=0.27-SNAPSHOT -Dpackaging=pom -DgeneratePom=true -DpomFile=pom.xml

cd hive-io-exp-core/target
mvn install:install-file -Dfile=hive-io-exp-core-0.27-SNAPSHOT.jar -DgroupId=com.facebook.hiveio -DartifactId=hive-io-exp-core -Dversion=0.27-SNAPSHOT -Dpackaging=jar -DgeneratePom=true -DpomFile=../pom.xml
mvn install:install-file -Dfile=hive-io-exp-core-0.27-SNAPSHOT-tests.jar -DgroupId=com.facebook.hiveio -DartifactId=hive-io-exp-core -Dversion=0.27-SNAPSHOT -Dpackaging=jar -Dclassifier=tests -DgeneratePom=true -DpomFile=../pom.xml

cd ../../hive-io-exp-testing/target
mvn install:install-file -Dfile=hive-io-exp-testing-0.27-SNAPSHOT.jar -DgroupId=com.facebook.hiveio -DartifactId=hive-io-exp-testing -Dversion=0.27-SNAPSHOT -Dpackaging=jar -DgeneratePom=true -DpomFile=../pom.xml

cd ../../../

#get, compile and install giraph

git clone git@bitbucket.org:m1lt0n/giraph.git

cd giraph
git checkout custom-maven-profile

mvn package -Pbitcoin-analysis -Dhadoop.version=${HADOOP_VERSION} -Ddep.hive.version=${HIVE_VERSION}

mvn install:install-file -Dfile=pom.xml -DgroupId=org.apache.giraph -DartifactId=giraph-parent -Dversion=1.1.0-SNAPSHOT -Dpackaging=pom -DgeneratePom=true -DpomFile=pom.xml

cd giraph-core/target/munged
mvn install:install-file -Dfile=${GIRAPH}.jar -DgroupId=org.apache.giraph -DartifactId=giraph-core -Dversion=1.1.0-SNAPSHOT -Dpackaging=jar-DpomFile=../../pom.xml -DgroupId=org.apache.giraph -DartifactId=giraph-core -Dversion=1.1.0-SNAPSHOT -Dpackaging=jar -DgeneratePom=true -DpomFile=../../pom.xml
mvn install:install-file -Dfile=${GIRAPH}-sources.jar -DgroupId=org.apache.giraph -DartifactId=giraph-core-test -Dversion=1.1.0-SNAPSHOT -Dpackaging=jar -DgeneratePom=true -DpomFile=../../pom.xml
mvn install:install-file -Dfile=${GIRAPH}-tests.jar -DgroupId=org.apache.giraph -DartifactId=giraph-core -Dversion=1.1.0-SNAPSHOT -Dpackaging=jar -Dclassifier=src -DgeneratePom=true -DpomFile=../../pom.xml

cd ../../../giraph-examples/target/munged
mvn install:install-file -Dfile=${GIRAPH_EXAMPLES}.jar -DgroupId=org.apache.giraph -DartifactId=giraph-examples -Dversion=1.1.0-SNAPSHOT -Dpackaging=jar -DgeneratePom=true -DpomFile=../../pom.xml
mvn install:install-file -Dfile=${GIRAPH_EXAMPLES}-sources.jar -DgroupId=org.apache.giraph -DartifactId=giraph-examples -Dversion=1.1.0-SNAPSHOT -Dpackaging=jar -Dclassifier=src -DgeneratePom=true -DpomFile=../../pom.xml

cd ../../../giraph-hive/target/
mvn install:install-file -Dfile=giraph-hive-1.1.0-SNAPSHOT.jar -DgroupId=org.apache.giraph -DartifactId=giraph-hive -Dversion=1.1.0-SNAPSHOT -Dpackaging=jar -DgeneratePom=true -DpomFile=../pom.xml

cd ../../../

#get, compile and install okapi

git clone https://github.com/grafos-ml/okapi.git

cd okapi
git checkout bb94586b1004808560b19df180ae334fce26ccf2

mvn package -Phadoop_yarn -Dhadoop.version=${HADOOP_VERSION} -DskipTests

cd target
mvn install:install-file -Dfile=okapi-0.3.5-SNAPSHOT.jar -DgroupId=ml.grafos.okapi -DartifactId=okapi -Dversion=0.3.5-SNAPSHOT -Dpackaging=jar -DgeneratePom=true -DpomFile=../pom.xml

cd ../../../
