/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

%default ti 'bitcoin-analysis/transactionInputs'
%default to 'bitcoin-analysis/transactionOutputs'

SET parquet.compression snappy;
SET mapred.compress.map.output true;
SET mapred.map.output.compression.codec org.apache.hadoop.io.compress.SnappyCodec;
--Caricamento transactionInputs da cambiare i tipi da chararray a bytearray
--A = LOAD 'data5' AS (transaction_hash:chararray, transaction_id:int, time:long, input_transaction_hash:chararray, output_index:int);
A = LOAD '$ti' USING parquet.pig.ParquetLoader
    AS (block_height:long, transaction_hash:bytearray, transaction_id:long, time:long, input_transaction_hash:bytearray, output_index:long);
A = FOREACH A GENERATE block_height, transaction_hash, (int)transaction_id, time, input_transaction_hash, (int)output_index;
/*Caricamento transactionOutputs da satoshis:long a  satoshis:biginteger??*/
--B = LOAD 'data6' AS (transaction_hash:chararray, transaction_id:int, time:long, address_hash:chararray, output_index:int, satoshis:long);
B = LOAD '$to' USING parquet.pig.ParquetLoader
    AS (block_height:long, transaction_hash:bytearray, transaction_id:long, time:long, address_hash:bytearray, output_index:long, satoshis:long);
B = FOREACH B GENERATE block_height, transaction_hash, (int)transaction_id, time, address_hash, (int)output_index, satoshis;
--Ricavo un lista di address distinti
Addresses = FOREACH B GENERATE address_hash;
Addresses = DISTINCT Addresses;
Addresses = RANK Addresses;
Addresses = FOREACH Addresses GENERATE (int)rank_Addresses as id, address_hash as hash ;

--Ricavo una serie di input transaction con associato l'id della trasazione in input e l'address con l'Id
C = join A by (input_transaction_hash, output_index), B by (transaction_hash, output_index);
C = join C by B::address_hash, Addresses by hash;
CompleteTransactionInputs = FOREACH C GENERATE
         A::block_height AS block_height, A::transaction_hash AS transaction_hash,
         A::transaction_id AS transaction_id, A::time AS time,
         A::input_transaction_hash AS input_transaction_hash, B::transaction_id AS input_transaction_id,
         A::output_index AS output_index, B::address_hash AS address_hash,
         Addresses::id AS address_id, B::satoshis AS satoshis;


--Ricavo delle output transaction con l'address Id
D = join B by address_hash, Addresses by hash;
CompleteTransactionOutputs = FOREACH D GENERATE
        block_height AS block_height, transaction_hash AS transaction_hash,
        transaction_id AS transaction_id, time AS time,
        B::address_hash AS address_hash, Addresses::id AS address_id,
        output_index AS output_index, B::satoshis AS satoshis;

-- Ricavo gli archi del grafo delle transazioni tra address
E = join CompleteTransactionOutputs by transaction_id, CompleteTransactionInputs by transaction_id;
X = FOREACH E GENERATE
        CompleteTransactionOutputs::transaction_id AS transaction_id,
        CompleteTransactionOutputs::time AS time,
        CompleteTransactionInputs::address_id AS from_address_id,
        CompleteTransactionOutputs::address_id AS to_address_id,
        CompleteTransactionOutputs::satoshis AS satoshis;
TransactionEdges = DISTINCT X;

-- Ricavo gli archi del grafo per ricavare le entità da poi usare per ridurre il grafo da usare
T = group CompleteTransactionInputs BY transaction_id;
T = FOREACH T GENERATE group AS transaction_id, FLATTEN(CompleteTransactionInputs.address_id) AS address_id;
T = join CompleteTransactionInputs by transaction_id, T by transaction_id;
T = FOREACH T GENERATE CompleteTransactionInputs::address_id AS from_address_id, T::address_id AS to_address_id;
EntitiesEdges = FILTER T BY from_address_id != to_address_id;
EntitiesEdges = DISTINCT EntitiesEdges;


store CompleteTransactionInputs into 'bitcoin-analysis/completeTransactionInputs/' USING parquet.pig.ParquetStorer;
store CompleteTransactionOutputs into 'bitcoin-analysis/completeTransactionOutputs/' USING parquet.pig.ParquetStorer;
store EntitiesEdges into 'bitcoin-analysis/entitiesEdges/' USING parquet.pig.ParquetStorer;
store TransactionEdges into 'bitcoin-analysis/transactionEdges/' USING parquet.pig.ParquetStorer;
store Addresses into 'bitcoin-analysis/addresses/' USING parquet.pig.ParquetStorer;

