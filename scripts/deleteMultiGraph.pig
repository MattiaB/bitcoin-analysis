/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

BTCtoUSD = LOAD 'bitcoin-analysis/btcToUsd' AS (day:long, exchange:double);
CompleteTransactionInputs = LOAD 'bitcoin-analysis/completeTransactionInputs/' USING parquet.pig.ParquetLoader;
CompleteTransactionOutputs = LOAD 'bitcoin-analysis/completeTransactionOutputs/' USING parquet.pig.ParquetLoader;
Entities = LOAD 'bitcoin-analysis/entitiesParquet' USING parquet.pig.ParquetLoader AS (address_id:int, entity_id:int);

A = join CompleteTransactionOutputs by transaction_id, CompleteTransactionInputs by transaction_id;

A = join A by CompleteTransactionInputs::address_id LEFT OUTER, Entities by address_id;

A = FOREACH A GENERATE
        CompleteTransactionOutputs::block_height AS block_height,
        CompleteTransactionOutputs::transaction_id AS transaction_id,
        ((Entities::entity_id IS NULL) ? CompleteTransactionInputs::address_id : Entities::entity_id ) AS from_entity_id,
        CompleteTransactionOutputs::address_id AS to_entity_id,
        CompleteTransactionOutputs::time AS time,
		CompleteTransactionOutputs::output_index AS output_index,
        CompleteTransactionOutputs::satoshis AS satoshis;

A = join A by to_entity_id  LEFT OUTER, Entities by address_id;

A = FOREACH A {
		day = (long)(A::time / 86400) * 86400000;
		
		GENERATE
		A::block_height AS block_height,
		A::transaction_id AS transaction_id,
		A::from_entity_id AS from_entity_id,
		((Entities::entity_id IS NULL) ? A::to_entity_id : Entities::entity_id )  AS to_entity_id,
		A::time AS time,
		A::output_index AS output_index,
		A::satoshis AS satoshis,
		day AS day;
};

A = join A by day, BTCtoUSD by day USING 'replicated';

A = FOREACH A {
		dollars = A::satoshis / 100000000.0f * BTCtoUSD::exchange;
		
		GENERATE
		A::block_height AS block_height,
		A::transaction_id AS transaction_id,
		A::from_entity_id AS from_entity_id,
		A::to_entity_id AS to_entity_id,
		A::time AS time,
		A::output_index AS output_index,
		A::satoshis AS satoshis,
		dollars AS dollars;
};


TransactionEdgesEntitiesDollars = DISTINCT A;

B = FILTER TransactionEdgesEntitiesDollars BY from_entity_id != to_entity_id;
B = GROUP B BY (from_entity_id,to_entity_id);
CompleteGraph = FOREACH B GENERATE
	group.from_entity_id as from_entity_id, group.to_entity_id as to_entity_id,
    SUM(B.dollars) AS dollars,
    SUM(B.satoshis) AS satoshis;


store CompleteGraph into 'bitcoin-analysis/completeGraph/' USING parquet.pig.ParquetStorer;
store TransactionEdgesEntitiesDollars into 'bitcoin-analysis/transactionEdgesEntitiesDollars/' USING parquet.pig.ParquetStorer;
