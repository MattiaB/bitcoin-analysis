/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.unito.bitcoin.blockchainimporter.blkreader;

import com.google.bitcoin.core.*;

import java.util.List;


/**
 * Implementors can be connected to a {@link FullBlocksBlockChain} and have its methods called when various things
 * happen that modify the state of the chain, for example: new blocks being received, a re-org occurring, or the
 * best chain head changing.
 */
public interface FullBlocksBlockChainListener {
    /**
     * <p>Called by the {@link FullBlocksBlockChain} when a new block on the best chain is seen, AFTER relevant
     * transactions are extracted and sent to us UNLESS the new block caused a re-org, in which case this will
     * not be called (the {@link FullBlocksBlockChainListener#reorganize(com.google.bitcoin.core.StoredBlock, java.util.List, java.util.List)} method will
     * call this one in that case).</p>
     *
     * @param block
     */
    void notifyNewBestBlock(StoredBlock storedBlock, Block block) throws VerificationException;

    /**
     * Called by the {@link FullBlocksBlockChain} when the best chain (representing total work done) has changed. In this case,
     * we need to go through our transactions and find out if any have become invalid. It's possible for our balance
     * to go down in this case: money we thought we had can suddenly vanish if the rest of the network agrees it
     * should be so.<p>
     * <p>
     * The oldBlocks/newBlocks lists are ordered height-wise from top first to bottom last.
     */
    void reorganize(StoredBlock splitPoint, List<StoredBlock> oldStoredBlocks,
                    List<StoredBlock> newStoredBlocks, List<Block> oldBlocks,
                    List<Block> newBlocks) throws VerificationException;

    /**
     * Returns true if the given transaction is interesting to the listener. If yes, then the transaction will
     * be provided via the receiveFromBlock method. This method is essentially an optimization that lets BlockChain
     * bypass verification of a blocks merkle tree if no listeners are interested, which can save time when processing
     * full blocks on mobile phones. It's likely the method will be removed in future and replaced with an alternative
     * mechanism that involves listeners providing all keys that are interesting.
     */
    boolean isTransactionRelevant(Transaction tx) throws ScriptException;

    /**
     * <p>Called by the {@link FullBlocksBlockChain} when we receive a new block that contains a relevant transaction.</p>
     * <p>
     * <p>A transaction may be received multiple times if is included into blocks in parallel chains. The blockType
     * parameter describes whether the containing block is on the main/best chain or whether it's on a presently
     * inactive side chain.</p>
     * <p>
     * <p>The relativityOffset parameter is an arbitrary number used to establish an ordering between transactions
     * within the same block. In the case where full blocks are being downloaded, it is simply the index of the
     * transaction within that block. When Bloom filtering is in use, we don't find out the exact offset into a block
     * that a transaction occurred at, so the relativity count is not reflective of anything in an absolute sense but
     * rather exists only to order the transaction relative to the others.</p>
     */
    void receiveFromBlock(Transaction tx, StoredBlock block,
                          FullBlocksBlockChain.NewBlockType blockType,
                          int relativityOffset) throws VerificationException;

    /**
     * <p>Called by the {@link FullBlocksBlockChain} when we receive a new {@link com.google.bitcoin.core.FilteredBlock} that contains the given
     * transaction hash in its merkle tree.</p>
     * <p>
     * <p>A transaction may be received multiple times if is included into blocks in parallel chains. The blockType
     * parameter describes whether the containing block is on the main/best chain or whether it's on a presently
     * inactive side chain.</p>
     * <p>
     * <p>The relativityOffset parameter in this case is an arbitrary (meaningless) number, that is useful only when
     * compared to the relativity count of another transaction received inside the same block. It is used to establish
     * an ordering of transactions relative to one another.</p>
     */
    void notifyTransactionIsInBlock(Sha256Hash txHash, StoredBlock block,
                                    FullBlocksBlockChain.NewBlockType blockType,
                                    int relativityOffset) throws VerificationException;

    void stop();
}

