/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.unito.bitcoin.blockchainimporter;

import com.google.bitcoin.core.*;
import com.google.bitcoin.script.Script;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import parquet.example.data.Group;
import parquet.example.data.simple.SimpleGroupFactory;
import parquet.hadoop.ParquetWriter;
import parquet.io.api.Binary;

import java.io.IOException;
import java.math.BigInteger;


public class DataStore {
    private static final Logger LOG = LoggerFactory.getLogger(DataStore.class);

    private final ParquetWriter<Group> inputTransactionTableWriter;
    private final SimpleGroupFactory inputTransactionGroupFactory;
    private final ParquetWriter<Group> outputTransactionTableWriter;
    private final SimpleGroupFactory outputTransactionGroupFactory;
    private final ParquetWriter<Group> blocksTableWriter;
    private final SimpleGroupFactory blocksGroupFactory;
    private final Statistic statistic;
    private long transactionId = 0;
	private long blockHeight = 0;


    public DataStore(ParquetWriter<Group> inputTransactionTableWriter,
                     SimpleGroupFactory inputTransactionGroupFactory,
                     ParquetWriter<Group> outputTransactionTableWriter,
                     SimpleGroupFactory outputTransactionGroupFactory,
                     ParquetWriter<Group> blocksTableWriter,
                     SimpleGroupFactory blocksGroupFactory,
                     Statistic statistic) {
        this.inputTransactionTableWriter = inputTransactionTableWriter;
        this.inputTransactionGroupFactory = inputTransactionGroupFactory;
        this.outputTransactionTableWriter = outputTransactionTableWriter;
        this.outputTransactionGroupFactory = outputTransactionGroupFactory;
        this.blocksTableWriter = blocksTableWriter;
        this.blocksGroupFactory = blocksGroupFactory;
        this.statistic = statistic;
    }

    public void insertTransaction(Transaction transaction, Block block) throws IOException {
        final byte[] transactionHash = transaction.getHash().getBytes();
        final long transactionId = this.transactionId;
        final long time = block.getTimeSeconds();

        for (TransactionInput transactionInput : transaction.getInputs()) {
            final TransactionOutPoint transactionOutPoint = transactionInput.getOutpoint();
            if (!transactionOutPoint.getHash().equals(Sha256Hash.ZERO_HASH)) {
                final byte[] inputTransactionHash = transactionOutPoint.getHash().getBytes();
                final long outputIndex = transactionOutPoint.getIndex();
                // Save
                Group group = inputTransactionGroupFactory.newGroup();
                group.add("block_height", blockHeight);
                group.add("transaction_hash", Binary.fromByteArray(transactionHash));
                group.add("transaction_id", transactionId);
                group.add("time", time);
                group.add("input_transaction_hash", Binary.fromByteArray(inputTransactionHash));
                group.add("output_index", outputIndex);
                inputTransactionTableWriter.write(group);
                statistic.addOneNumInputTransaction();
            }
        }
        long outputIndexCounter = 0;
        for (TransactionOutput transactionOutput : transaction.getOutputs()) {

            final byte[] addressHash = convertScriptIntoAddressBytes(
                    transactionOutput.getScriptPubKey()
            );
            final BigInteger satoshi = transactionOutput.getValue();
            //Save
            Group group = outputTransactionGroupFactory.newGroup();
            group.add("block_height", blockHeight);
            group.add("transaction_hash", Binary.fromByteArray(transactionHash));
            group.add("transaction_id", transactionId);
            group.add("time", time);
            group.add("address_hash", Binary.fromByteArray(addressHash));
            group.add("output_index", outputIndexCounter);
            group.add("satoshis", satoshi.longValue());
            outputTransactionTableWriter.write(group);
            statistic.addOneNumOutputTransaction();
            outputIndexCounter += 1;
        }

        this.transactionId++;
    }
    
    public void insertBlock(Block block) throws IOException {
    	try{
	    	final byte[] blockHash = block.getHash().getBytes();
	        final long difficulty = block.getDifficultyTarget();
            final long work = block.getWork().longValue();
            final byte[] prevBlockHash = block.getPrevBlockHash().getBytes();
            final byte[] merkleRoot = block.getMerkleRoot().getBytes();
            final long height = this.blockHeight;
	        final long time = block.getTimeSeconds();
        
	        //Save
	        Group group = blocksGroupFactory.newGroup();
	        group.add("block_hash", Binary.fromByteArray(blockHash));
            group.add("prevBlockHash", Binary.fromByteArray(prevBlockHash));
            group.add("merkleRoot", Binary.fromByteArray(merkleRoot));
	        group.add("height", height);
	        group.add("time", time);
            group.add("difficulty", difficulty);
            group.add("work", work);
	        blocksTableWriter.write(group);
	    } catch (Exception e) {
	        throw e;
	    }
        
        this.blockHeight++;
    }

    public static byte[] convertScriptIntoAddressBytes(Script script) {
        byte[] addressKey;
        if (script.isSentToAddress()) {
            addressKey = script.getPubKeyHash();
        } else {
            addressKey = Utils.sha256hash160(script.getPubKey());
        }
        return addressKey;
    }
}
