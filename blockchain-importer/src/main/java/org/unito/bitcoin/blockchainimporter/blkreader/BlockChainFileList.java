/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.unito.bitcoin.blockchainimporter.blkreader;

import java.io.File;
import java.util.LinkedList;
import java.util.List;


public class BlockChainFileList {
    public static List<File> getList(String dataDir) {
        List<File> clientBlockFileList = new LinkedList<File>();
        for (int i = 0; true; i++) {
            File file = new File(dataDir + String.format("blk%05d.dat", i));
            if (!file.exists())
                break;
            clientBlockFileList.add(file);
        }
        return clientBlockFileList;
    }
}
