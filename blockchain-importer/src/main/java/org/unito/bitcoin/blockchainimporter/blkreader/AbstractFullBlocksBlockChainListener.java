/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.unito.bitcoin.blockchainimporter.blkreader;

import com.google.bitcoin.core.*;

import java.util.List;


public abstract class AbstractFullBlocksBlockChainListener implements FullBlocksBlockChainListener {
    @Override
    public void notifyNewBestBlock(StoredBlock storedBlock, Block block) throws VerificationException {
    }

    @Override
    public void reorganize(StoredBlock splitPoint, List<StoredBlock> oldStoredBlocks,
                           List<StoredBlock> newStoredBlocks, List<Block> oldBlocks,
                           List<Block> newBlocks) throws VerificationException {
    }

    @Override
    public boolean isTransactionRelevant(Transaction tx) throws ScriptException {
        return false;
    }

    @Override
    public void receiveFromBlock(Transaction tx, StoredBlock block, FullBlocksBlockChain.NewBlockType blockType,
                                 int relativityOffset) throws VerificationException {
    }

    @Override
    public void notifyTransactionIsInBlock(Sha256Hash txHash, StoredBlock block, FullBlocksBlockChain.NewBlockType blockType,
                                           int relativityOffset) throws VerificationException {
    }

    @Override
    public void stop() {
    }
}
