/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.unito.bitcoin.blockchainimporter.blkreader;

import com.google.bitcoin.core.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MainChainIteratorListener implements FullBlocksBlockChainListener,
        Iterable<Block> {
    private int minLength; //minimum length of currentChain before we should return a block with next()
    private int maxLength; //maximum length of currentChain we should reach
    private int maxHeight; //maximum height of the blockchain we want to reach
    private int currentHeight; //current height of the blockchain
    private List<Block> currentChain; //list of blocks in the current best chain

    //lock & conditions for syncing
    private Lock lock;
    private Condition minChainLength;
    private Condition maxChainLength;
    private boolean stop;

    public MainChainIteratorListener(int minLength, int maxLength, int maxHeight) {
        this.minLength = minLength;
        this.maxLength = maxLength;
        this.maxHeight = maxHeight;
        this.currentChain = new ArrayList<>();
        this.lock = new ReentrantLock();
        this.minChainLength = lock.newCondition();
        this.maxChainLength = lock.newCondition();
    }

    public int getCurrentChainSize() {
        lock.lock();
        try {
            return currentChain.size();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void notifyNewBestBlock(StoredBlock storedBlock, Block block)
            throws VerificationException {
        lock.lock();
        try {
            while (currentChain.size() >= maxLength & !stop) {
                try {
                    maxChainLength.await();
                } catch (InterruptedException e) {
                    //TODO: handle this better? (i.e. unlock)
                    throw new RuntimeException(e); //should never happen
                }
            }
            if (!stop) {
                //add to currentChain
                currentChain.add(block);
                //update currentHeight
                currentHeight = storedBlock.getHeight();
                //signal if appropriate
                if (currentChain.size() > minLength) {
                    minChainLength.signal();
                }
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void reorganize(StoredBlock splitPoint, List<StoredBlock> oldStoredBlocks,
                           List<StoredBlock> newStoredBlocks, List<Block> oldBlocks,
                           List<Block> newBlocks) throws VerificationException {
        lock.lock();
        try {
            //delete oldBlocks from currentChain
            for (Block block : oldBlocks) {
                boolean found = currentChain.remove(block);
                if (!found)
                    throw new IllegalStateException("oldBlock " + block.getHash() + " was not found in currentChain");
            }
            //add newBlocks to currentChain
            for (int i = newBlocks.size() - 1; i >= 0; i--) {
                while (currentChain.size() >= maxLength & !stop) {
                    try {
                        maxChainLength.await();
                    } catch (InterruptedException e) {
                        //TODO: handle this better? (i.e. unlock)
                        throw new RuntimeException(e); //should never happen
                    }
                }
                if (!stop) {
                    //add to currentChain
                    currentChain.add(newBlocks.get(i));
                    //update currentHeight
                    currentHeight = newStoredBlocks.get(i).getHeight();
                }
            }
            //signal if appropriate
            if (currentChain.size() > minLength) {
                minChainLength.signal();
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean isTransactionRelevant(Transaction tx) throws ScriptException {
        return false; //we don't care about any transaction
    }

    @Override
    public void receiveFromBlock(Transaction tx, StoredBlock block, FullBlocksBlockChain.NewBlockType blockType, int relativityOffset)
            throws VerificationException {
        return; //we don't care about transactions
    }

    @Override
    public void notifyTransactionIsInBlock(Sha256Hash txHash, StoredBlock block, FullBlocksBlockChain.NewBlockType blockType, int relativityOffset)
            throws VerificationException {
        return; //we don't care about transactions
    }

    @Override
    public Iterator<Block> iterator() {
        return new FullBlocksIterator();
    }

    @Override
    public void stop() {
        stop = true;
        lock.lock();
        minChainLength.signal();
        maxChainLength.signal();
        lock.unlock();
    }

    private class FullBlocksIterator implements Iterator<Block> {
        @Override
        public boolean hasNext() {
            lock.lock();
            try {
                return ((currentHeight - currentChain.size()) < maxHeight) & !stop;
            } finally {
                lock.unlock();
            }
        }

        @Override
        public Block next() {
            lock.lock();
            try {
                while (currentChain.size() <= minLength & !stop) {
                    try {
                        minChainLength.await();
                    } catch (InterruptedException e) {
                        //TODO: handle this better? (i.e. unlock)
                        throw new RuntimeException(e); //should never happen
                    }
                }
                if (!stop) {
                    Block next = currentChain.remove(0);
                    if (currentChain.size() < maxLength) {
                        maxChainLength.signal();
                    }
                    return next;
                } else return null;
            } finally {
                lock.unlock();
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
