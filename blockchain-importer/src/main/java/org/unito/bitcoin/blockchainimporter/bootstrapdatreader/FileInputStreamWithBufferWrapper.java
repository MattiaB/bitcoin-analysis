/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.unito.bitcoin.blockchainimporter.bootstrapdatreader;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class FileInputStreamWithBufferWrapper {
    private static int BIG_SIZE = 6 * 1000 * 1000; // 3MB
    private final ByteBuffer buffer;
    private final FileChannel fileChannel;

    public FileInputStreamWithBufferWrapper(File fileName) throws IOException {

        FileInputStream bootstrapDatFileStream = new FileInputStream(fileName);

        fileChannel = bootstrapDatFileStream.getChannel();
        buffer = ByteBuffer.allocateDirect(BIG_SIZE);
        rebuildBuffer();
    }

    public int getInt() throws IOException {
        if (hasRemaining()) {
            return ((int) buffer.get()) & 0xff;
        } else {
            throw new RuntimeException();
        }
    }

    public void get(byte[] dst, int length) throws IOException {
        int bufferRemaining = buffer.remaining();
        int offset = 0;
        if (length > bufferRemaining) {
            if (bufferRemaining > 0) {
                buffer.get(dst, 0, bufferRemaining);
                offset = bufferRemaining;
            }
            rebuildBuffer();
        }
        buffer.get(dst, offset, length - offset);
    }

    public boolean hasRemaining() throws IOException {
        if (buffer.hasRemaining()) {
            return true;
        } else {
            int nRead = rebuildBuffer();
            return nRead != -1;
        }
    }

    private int rebuildBuffer() throws IOException {
        buffer.clear();
        int nRead;
        while ((nRead = fileChannel.read(buffer)) != -1) {
            if (nRead == 0)
                continue;
            buffer.position(0);
            buffer.limit(nRead);
            break;
        }
        return nRead;
    }

    public static int byteArrayToInt(byte[] b)
    {
        int value = 0;
        for (int i = 0; i < 4; i++) {
            int shift = (4 - 1 - i) * 8;
            value += (b[i] & 0x000000FF) << shift;
        }
        return value;
    }
}
