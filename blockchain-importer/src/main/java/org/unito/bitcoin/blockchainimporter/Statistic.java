/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.unito.bitcoin.blockchainimporter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Statistic {
    private static final Logger log = LoggerFactory.getLogger(Statistic.class);

    private long statsLastTime = System.currentTimeMillis();
    private int speedTransactions = 0;
    private int speedBlocks = 0;
    private int numTransactions = 0;
    private int numProblematicTransactions = 0;
    private int numBlocks = 0;
    private int numProblematicBlocks = 0;
    private int numProblematicOutputTransaction = 0;
    private int numInputTransaction = 0;
    private int numOutputTransaction = 0;

    public void showTimePerformanceStats() {
        if (System.currentTimeMillis() - statsLastTime > 10000) { // more than 10s passed since last stats logging.
            log.info("Performance stats: {} blocks per second; {} transactions per second.", speedBlocks/10, speedTransactions/10);
            statsLastTime = System.currentTimeMillis();
            speedBlocks = 0;
            speedTransactions = 0;
        }
    }
    
    public void showGlobalStats() {
    	log.info("Global stats: {} blocks; " +
                "{} transactions; " +
                "{} input transactions; " +
                "{} output transactions; " +
                "{} problematic blocks; " +
                "{} problematic transactions." +
                "{} problematic address/output transactions.", numBlocks, numTransactions,
                numInputTransaction, numOutputTransaction, numProblematicBlocks,
                numProblematicTransactions, numProblematicOutputTransaction);
    }

    public void addOneTransaction() {
        numTransactions++;
        speedTransactions++;
    }
    
    public void addOneProblematicTransaction() {
	    numProblematicTransactions++;
    }

	public void addOneBlock() {
	    numBlocks++;
	    speedBlocks++;
    }

	public void addOneProblematicBlock() {
	    numProblematicBlocks++;
    }

    public void addOneProblematicOutputTransaction() {
        this.numProblematicOutputTransaction++;
    }

    public void addOneNumInputTransaction() {
        this.numInputTransaction++;
    }

    public void addOneNumOutputTransaction() {
        this.numOutputTransaction++;
    }
}
