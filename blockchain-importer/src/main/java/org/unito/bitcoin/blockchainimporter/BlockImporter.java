/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.unito.bitcoin.blockchainimporter;

import com.google.bitcoin.core.*;
import com.google.bitcoin.params.MainNetParams;
import com.google.bitcoin.utils.BlockFileLoader;
import org.apache.commons.cli.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unito.bitcoin.blockchainimporter.blkreader.*;
import org.unito.bitcoin.blockchainimporter.bootstrapdatreader.BootstrapDatLoader;
import parquet.example.data.Group;
import parquet.example.data.simple.SimpleGroupFactory;
import parquet.hadoop.ParquetWriter;
import parquet.hadoop.metadata.CompressionCodecName;
import parquet.schema.MessageType;
import parquet.schema.MessageTypeParser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class BlockImporter {
    private static Options OPTIONS;

    static {
        OPTIONS = new Options();
        OPTIONS.addOption("h", "help", false, "print this help and exit");
        OPTIONS.addOption("n", "numBlocks", true, "number of blocks to import. NOTE: "
                + "when loading from blk files, the last 100 blocks in the blk files "
                + "will not be importable due to limitations in the main chain detection "
                + "algorithm");
        OPTIONS.addOption("co", "parquetCompression", true, "Compression for parquet files; choose between gzip,snappy,lzo");
        OPTIONS.addOption("bs", "parquetBlockSize", true, "Parquet Block size in byte");
        OPTIONS.addOption("ps", "parquetPageSize", true, "Parquet Page size in byte");
        OPTIONS.addOption("ti", "transactionInputsTablePath", true, "Path for parquet transaction inputs table, " +
                "it can be a hdfs path, for example: hdfs://localhost/user/username/transactionInputs");
        OPTIONS.addOption("to", "transactionOutputsTablePath", true, "Path for parquet transaction outputs table, " +
                "it can be a hdfs path, for example: hdfs://localhost/user/username/transactionOutputs");
        OPTIONS.addOption("b", "blocksTablePath", true, "Path for parquet blocks table, " +
                "it can be a hdfs path, for example: hdfs://localhost/user/username/blocks");
        OptionGroup inputData = new OptionGroup();
        Option datFile = new Option("df", "datFile", true, "bitcoin bootstrap.dat "
                + "file to read blocks from");
        Option blkDirectory = new Option("bd", "blkDirectory", true, "bitcoin blk "
                + "files directory to read blocks from");
        inputData.addOption(datFile);
        inputData.addOption(blkDirectory);
        inputData.setRequired(true);
        OPTIONS.addOptionGroup(inputData);
    }

    public static void main(String[] args) throws Exception {
        final Logger log = LoggerFactory.getLogger(BlockImporter.class);
        final String transactionInputsTablePath;
        final String transactionOtputsTablePath;
        final String blocksTablePath;
        final CompressionCodecName parquetCompression;
        final int parquetBlockSize, parquetPageSize;

        final int numBlocks;

        final NetworkParameters params = MainNetParams.get();
        final Iterable<Block> blocksIterator;
        Thread chainThread = null;

        CommandLineParser parser = new BasicParser();
        try {
            CommandLine cmd = parser.parse(OPTIONS, args);
            if (cmd == null || cmd.hasOption("h")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp(BlockImporter.class.getName(), OPTIONS, true);
                System.exit(0);
            }
            if (cmd.hasOption("n")) {
                numBlocks = Integer.valueOf(cmd.getOptionValue("n"));
            } else {
                numBlocks = Integer.MAX_VALUE;
            }

            if (cmd.hasOption("ti")) {
                transactionInputsTablePath = cmd.getOptionValue("ti");
            } else {
                transactionInputsTablePath = "bitcoin-analysis/transactionInputs/";
            }

            if (cmd.hasOption("to")) {
                transactionOtputsTablePath = cmd.getOptionValue("to");
            } else {
                transactionOtputsTablePath = "bitcoin-analysis/transactionOutputs/";
            }
            if (cmd.hasOption("b")) {
                blocksTablePath = cmd.getOptionValue("b");
            } else {
                blocksTablePath = "bitcoin-analysis/blocks/";
            }

            if (cmd.hasOption("co")) {
                String option = cmd.getOptionValue("co").toLowerCase().trim();
                switch (option) {
                    case "gzip":
                        parquetCompression = CompressionCodecName.GZIP;
                        break;
                    case "snappy":
                        parquetCompression = CompressionCodecName.SNAPPY;
                        break;
                    case "lzo":
                        parquetCompression = CompressionCodecName.LZO;
                        break;
                    default:
                        throw new RuntimeException("Parquet compression option not valid: choose between gzip,snappy,lzo");
                }
            } else {
                parquetCompression = CompressionCodecName.SNAPPY;
            }
            if (cmd.hasOption("bs")) {
                parquetBlockSize = Integer.valueOf(cmd.getOptionValue("bs").trim());
            } else {
                parquetBlockSize = ParquetWriter.DEFAULT_BLOCK_SIZE;
            }
            if (cmd.hasOption("ps")) {
                parquetPageSize = Integer.valueOf(cmd.getOptionValue("ps").trim());
            } else {
                parquetPageSize =  ParquetWriter.DEFAULT_PAGE_SIZE;
            }

            if (cmd.hasOption("df")) {
                File bootstrapDat = new File(cmd.getOptionValue("df"));
                log.info("Using bootstrap.dat file from " + bootstrapDat.getAbsolutePath());
                blocksIterator = new BootstrapDatLoader(params, bootstrapDat);
            } else if (cmd.hasOption("bd")) {
                String blkFilesDir = cmd.getOptionValue("bd");
                List<File> clientBlockFileList = BlockChainFileList.getList(blkFilesDir);
                log.info("Using " + clientBlockFileList.size() + " blk files from " + (new File(blkFilesDir)).getAbsolutePath());
                final BlockFileLoader blkFilesLoader = new BlockFileLoader(params, clientBlockFileList);

                MainChainIteratorListener listener = new MainChainIteratorListener(100, 200, 300000);
                List<FullBlocksBlockChainListener> listeners = new ArrayList<>();
                listeners.add(listener);
                final FullBlocksBlockChain chain = new FullBlocksBlockChain(params, listeners, new MemoryFullBlockStore(params));
                chainThread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        log.info("[Chain Thread] Chain thread started");
                        int blocks = 0;
                        long startTime = System.nanoTime();
                        for (Block block : blkFilesLoader) {
                            try {
                                chain.add(block);
                            } catch (VerificationException | PrunedException e) {
                                //TODO: handle this better?
                                throw new RuntimeException(e); //should never happen
                            }
                            blocks++;
                        }
                        long endTime = System.nanoTime();
                        long time = (endTime - startTime) / 1000000000;
                        chain.stopListeners();
                        log.info("[Chain Thread] Done. Read " + blocks + " blocks from blk files in " + time / 3600 + ":" + time / 60 + ":" + time % 60);
                        log.info("[Chain Thread] Chain thread stopped");
                    }
                });
                chainThread.start();

                blocksIterator = listener;
            } else {
                throw new RuntimeException("No input data was specified!");
            }
        } catch (Exception e) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(BlockImporter.class.getName(), OPTIONS, true);
            throw new RuntimeException(e);
        }

        long saveLastTime = System.currentTimeMillis();

        final Statistic statistic = new Statistic();


        final Configuration conf = new Configuration();
        conf.setStrings("fs.file.impl", "org.apache.hadoop.fs.LocalFileSystem");
        conf.setStrings("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");

        final String transactionInputsTableMessage =
                "message transactionInputs {\n" +
                "    required int64 block_height;\n" +
                "    required binary transaction_hash;\n" +
                "    required int64 transaction_id;\n" +
                "    required int64 time;\n" +
                "    required binary input_transaction_hash;\n" +
                "    required int64 output_index;\n" +
                "}";
        final String transactionOutputsTableMessage =
                "message transactionOutputs {\n" +
                "    required int64 block_height;\n" +
                "    required binary transaction_hash;\n" +
                "    required int64 transaction_id;\n" +
                "    required int64 time;\n" +
                "    required binary address_hash;\n" +
                "    required int64 output_index;\n" +
                "    required int64 satoshis;\n" +
                "}";
        final String blocksTableMessage =
                "message blocks {\n" +
                "    required binary block_hash;\n" +
                "    required binary prevBlockHash;\n" +
                "    required binary merkleRoot;\n" +
                "    required int64 height;\n" +
                "    required int64 time;\n" +
                "    required int64 difficulty;\n" +
                "    required int64 work;\n" +
                "}";
        
        MessageType transactionInputsMessageType = MessageTypeParser.parseMessageType(transactionInputsTableMessage);
        SimpleGroupFactory transactionInputsGroupFactory = new SimpleGroupFactory(transactionInputsMessageType);
        ParquetWriter<Group> transactionInputsTableWriter =
                new ParquetWriter<>(
                        new Path(transactionInputsTablePath),
                        new GroupWriteSupportWithSchema(transactionInputsMessageType),
                        parquetCompression, parquetBlockSize, parquetPageSize,
                        parquetPageSize, true, false, conf
                );

        MessageType transactionOutputsMessageType = MessageTypeParser.parseMessageType(transactionOutputsTableMessage);
        SimpleGroupFactory transactionOutputsGroupFactory = new SimpleGroupFactory(transactionOutputsMessageType);
        ParquetWriter<Group> transactionOutputsTableWriter =
                new ParquetWriter<>(
                        new Path(transactionOtputsTablePath),
                        new GroupWriteSupportWithSchema(transactionOutputsMessageType),
                        parquetCompression, parquetBlockSize, parquetPageSize,
                        parquetPageSize, true, false, conf
                );
        
        MessageType blocksMessageType = MessageTypeParser.parseMessageType(blocksTableMessage);
        SimpleGroupFactory blocksGroupFactory = new SimpleGroupFactory(blocksMessageType);
        ParquetWriter<Group> blocksTableWriter =
                new ParquetWriter<>(
                        new Path(blocksTablePath),
                        new GroupWriteSupportWithSchema(blocksMessageType),
                        parquetCompression, parquetBlockSize, parquetPageSize,
                        parquetPageSize, true, false, conf
                );
        
        final DataStore dataStore = new DataStore(
                transactionInputsTableWriter,
                transactionInputsGroupFactory,
                transactionOutputsTableWriter,
                transactionOutputsGroupFactory,
                blocksTableWriter,
                blocksGroupFactory,
                statistic
        );

        try {
            int blocks = 0;
            long startTime = System.nanoTime();
            for (Block block : blocksIterator) {
                if (blocks >= numBlocks) {
                    break;
                }
                
                if (block == null) {
                	statistic.addOneProblematicBlock();
                    continue;
                }
                
                try {
	                dataStore.insertBlock(block);
                    statistic.addOneBlock();
                } catch (ScriptException | ProtocolException e) {
                	log.warn("Got exception at block hash {}: {}", block.getHashAsString(), e.getMessage());
                	statistic.addOneProblematicBlock();
                } catch (Exception e) {
                    log.error("Got exception at block hash {}: {}", block.getHashAsString(), e.getMessage());
                    throw e;
                }
                
                for (Transaction transaction : block.getTransactions()) {
                    try {
                        dataStore.insertTransaction(transaction, block);
                        statistic.addOneTransaction();
                    } catch (ScriptException e) {
                    	log.warn("Got exception at block hash {}, transaction hash {}: {}", block.getHashAsString(), transaction.getHashAsString(), e.getMessage());
                    	statistic.addOneProblematicTransaction();
                    } catch (Exception e) {
                        log.error("Got exception at block hash {}, transaction hash {}: {}", block.getHashAsString(), transaction.getHashAsString(), e.getMessage());
                        throw e;
                    }
                }
                
                statistic.showTimePerformanceStats();
                if (System.currentTimeMillis() - saveLastTime > 60000) {
                    saveLastTime = System.currentTimeMillis();
                    log.info("Current block height: {}, date: {}", blocks, block.getTime().toString());
                }

                blocks++;
            }
            
            statistic.showGlobalStats();
            long endTime = System.nanoTime();
            long time = (endTime - startTime) / 1000000000;
            log.info("Done. Total time: " + time / 3600 + ":" + time / 60 + ":" + time % 60);
            
            if (chainThread != null) {
                chainThread.interrupt();
            }

        } finally {
            transactionInputsTableWriter.close();
            transactionOutputsTableWriter.close();
            blocksTableWriter.close();
        }
    }
}