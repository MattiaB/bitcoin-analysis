/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.unito.bitcoin.blockchainimporter.blkreader;

import com.google.bitcoin.core.Block;
import com.google.bitcoin.core.Sha256Hash;
import com.google.bitcoin.core.StoredBlock;
import com.google.bitcoin.store.BlockStore;
import com.google.bitcoin.store.BlockStoreException;

public interface FullBlockStore extends BlockStore {
    /**
     * Saves the given block+extra data. The key isn't specified explicitly as it can be calculated from the
     * {@link com.google.bitcoin.core.Block} directly. Can throw if there is a problem with the underlying storage layer such as running out of
     * disk space.
     */
    void put(StoredBlock storedBlock, Block fullBlock) throws BlockStoreException;

    /**
     * Returns the Block given a hash. The returned values block.getHash() method will be equal to the
     * parameter. If no such block is found, returns null.
     */
    Block getFull(Sha256Hash hash) throws BlockStoreException;

    /**
     * Returns the {@link com.google.bitcoin.core.Block} that represents the top of the chain of greatest total work. Note that this
     * can be arbitrarily expensive, you probably should use {@link com.google.bitcoin.core.BlockChain#getChainHead()}
     * or perhaps {@link com.google.bitcoin.core.BlockChain#getBestChainHeight()} which will run in constant time and
     * not take any heavyweight locks.
     */
    Block getChainHeadFull() throws BlockStoreException;

    /**
     * Sets the {@link com.google.bitcoin.core.Block} that represents the top of the chain of greatest total work.
     */
    void setChainHead(StoredBlock storedChainHead, Block fullChainHead) throws BlockStoreException;
}
