/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.unito.bitcoin.blockchainimporter.blkreader;

import com.google.bitcoin.core.*;
import com.google.bitcoin.store.BlockStoreException;

import java.util.LinkedHashMap;
import java.util.Map;

public class MemoryFullBlockStore implements FullBlockStore {
    private LinkedHashMap<Sha256Hash, StoredBlock> storedBlockMap = new LinkedHashMap<Sha256Hash, StoredBlock>() {
        @Override
        protected boolean removeEldestEntry(Map.Entry<Sha256Hash, StoredBlock> eldest) {
            return storedBlockMap.size() > 2500;
        }
    };
    private LinkedHashMap<Sha256Hash, Block> fullBlockMap = new LinkedHashMap<Sha256Hash, Block>() {
        @Override
        protected boolean removeEldestEntry(Map.Entry<Sha256Hash, Block> eldest) {
            return fullBlockMap.size() > 500;
        }
    };
    private StoredBlock storedChainHead;
    private Block fullChainHead;

    public MemoryFullBlockStore(NetworkParameters params) {
        // Insert the genesis block.
        try {
            Block genesisBlock = params.getGenesisBlock();
            Block genesisHeader = genesisBlock.cloneAsHeader();
            StoredBlock genesisStoredBlock = new StoredBlock(genesisHeader, genesisHeader.getWork(), 0);
            put(genesisStoredBlock, genesisBlock);
            setChainHead(genesisStoredBlock, genesisBlock);
        } catch (BlockStoreException e) {
            throw new RuntimeException(e);  // Cannot happen.
        } catch (VerificationException e) {
            throw new RuntimeException(e);  // Cannot happen.
        }
    }

    @Override
    public void put(StoredBlock storedBlock, Block fullBlock)
            throws BlockStoreException {
        if (storedBlockMap == null || fullBlockMap == null)
            throw new BlockStoreException("MemoryBlockStore is closed");
        Sha256Hash storedBlockHash = storedBlock.getHeader().getHash();
        Sha256Hash fullBlockHash = fullBlock.getHash();
        if (!storedBlockHash.equals(fullBlockHash))
            throw new IllegalArgumentException("StoredBlock hash and Block hash are different");
        storedBlockMap.put(storedBlockHash, storedBlock);
        fullBlockMap.put(storedBlockHash, fullBlock);
    }

    @Override
    public StoredBlock get(Sha256Hash hash) throws BlockStoreException {
        if (storedBlockMap == null || fullBlockMap == null)
            throw new BlockStoreException("MemoryBlockStore is closed");
        return storedBlockMap.get(hash);
    }

    @Override
    public Block getFull(Sha256Hash hash) throws BlockStoreException {
        if (storedBlockMap == null || fullBlockMap == null)
            throw new BlockStoreException("MemoryBlockStore is closed");
        return fullBlockMap.get(hash);
    }

    @Override
    public StoredBlock getChainHead() throws BlockStoreException {
        if (storedBlockMap == null || fullBlockMap == null)
            throw new BlockStoreException("MemoryBlockStore is closed");
        return storedChainHead;
    }

    @Override
    public void setChainHead(StoredBlock chainHead) throws BlockStoreException {
        throw new UnsupportedOperationException("Only calls to setChainHead(StoredBlock, Block) are allowed!");
    }

    @Override
    public Block getChainHeadFull() throws BlockStoreException {
        if (storedBlockMap == null || fullBlockMap == null)
            throw new BlockStoreException("MemoryBlockStore is closed");
        return fullChainHead;
    }

    @Override
    public void setChainHead(StoredBlock storedChainHead, Block fullChainHead)
            throws BlockStoreException {
        Sha256Hash storedBlockHash = storedChainHead.getHeader().getHash();
        Sha256Hash fullBlockHash = fullChainHead.getHash();
        if (!storedBlockHash.equals(fullBlockHash))
            throw new IllegalArgumentException("StoredBlock hash and Block hash are different");
        this.storedChainHead = storedChainHead;
        this.fullChainHead = fullChainHead;
    }

    @Override
    public void close() throws BlockStoreException {
        storedBlockMap = null;
        fullBlockMap = null;
    }

    @Override
    public void put(StoredBlock block) throws BlockStoreException {
        throw new UnsupportedOperationException("Only calls to put(StoredBlock, Block) are allowed!");
    }
}
