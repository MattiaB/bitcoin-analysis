/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.unito.bitcoin.blockchainimporter.bootstrapdatreader;

import com.google.bitcoin.core.Block;
import com.google.bitcoin.core.NetworkParameters;
import com.google.bitcoin.core.ProtocolException;
import com.google.bitcoin.core.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * <p>This class reads block files stored in the reference/Satoshi client format. This is simply a way to concatenate
 * blocks together. Importing block data with this tool can be a lot faster than syncing over the network, if you
 * have the files available.</p>
 * <p>
 * <p>In order to comply with Iterator&lt;Block>, this class swallows a lot of IOExceptions, which may result in a few
 * blocks being missed followed by a huge set of orphan blocks.</p>
 * <p>
 * <p>To blindly import all files which can be found in a reference client (version >= 0.8) datadir automatically,
 * try this code fragment:<br>
 * BlockFileLoader loader = new BlockFileLoader(BlockFileLoader.getReferenceClientBlockFileList());<br>
 * for (Block block : loader) {<br>
 * &nbsp;&nbsp;try { chain.add(block); } catch (Exception e) { }<br>
 * }</p>
 */
public class BootstrapDatLoader implements Iterable<Block>, Iterator<Block> {
    private byte[] blockLength;
    private NetworkParameters params;
    private FileInputStreamWithBufferWrapper bootstrapDatFileStream = null;
    private Block nextBlock = null;

    public BootstrapDatLoader(NetworkParameters params, File bootstrapDat) {
        this.params = params;
        try {
            bootstrapDatFileStream = new FileInputStreamWithBufferWrapper(bootstrapDat);
            blockLength = new byte[4];
        } catch (IOException e) {
            bootstrapDatFileStream = null;
        }
    }

    @Override
    public boolean hasNext() {
        if (nextBlock == null)
            loadNextBlock();
        return nextBlock != null;
    }

    @Override
    public Block next() throws NoSuchElementException {
        if (!hasNext())
            throw new NoSuchElementException();
        Block next = nextBlock;
        nextBlock = null;
        return next;
    }

    private void loadNextBlock() {
        while (true) {
            try {
                //check filestream is not null and not finished
                if (bootstrapDatFileStream == null || !bootstrapDatFileStream.hasRemaining()) {
                    bootstrapDatFileStream = null;
                    break;
                }

                //load block
                //find and read network magic packet number
                int nextChar = bootstrapDatFileStream.getInt();

                while (nextChar != -1) {
                    if (nextChar != ((params.getPacketMagic() >>> 24) & 0xff)) {
                        nextChar = bootstrapDatFileStream.getInt();

                        continue;
                    }
                    nextChar = bootstrapDatFileStream.getInt();

                    if (nextChar != ((params.getPacketMagic() >>> 16) & 0xff))
                        continue;
                    nextChar = bootstrapDatFileStream.getInt();
                    if (nextChar != ((params.getPacketMagic() >>> 8) & 0xff))
                        continue;
                    nextChar = bootstrapDatFileStream.getInt();
                    if (nextChar == (params.getPacketMagic() & 0xff))
                        break;
                }

                //read length
                bootstrapDatFileStream.get(blockLength, 4);
                long size = Utils.readUint32BE(Utils.reverseBytes(blockLength), 0);

                // We allow larger than MAX_BLOCK_SIZE because test code uses this as well.
                if (size > Block.MAX_BLOCK_SIZE * 2 || size <= 0)
                    continue;
                //read block
                byte[] bytes = new byte[(int) size];
                bootstrapDatFileStream.get(bytes, (int) size);
                try {
                    nextBlock = new Block(params, bytes, true, false, bytes.length);
                } catch (ProtocolException e) {
                    nextBlock = null;
                    continue;
                }
                break;
            } catch (IOException e) {
                bootstrapDatFileStream = null;
            }
        }
    }

    @Override
    public void remove() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<Block> iterator() {
        return this;
    }
}
