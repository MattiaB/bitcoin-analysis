#!/bin/bash
# Copyright Mattia Bertorello, Nicolò Bidotti 2014
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU  Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU  Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

usage="Usage: ./mount-tables.sh [-h] -u hdfs_user

Where:
	-h               \t print this help and exit.
	-u hdfs_user     \t used for table paths; will mount tables in 
	                 \t \"hdfs:///user/hdfs_user/bitcoin-analysis/
\n"

#get arguments and check them
while test $# -gt 0
do
	case "$1" in
		-h)
			printf "$usage"
			exit 0
			;;
		-u)
			shift
			hdfs_user=$1
			shift
			;;
		*)
			break
			;;
	esac
done
if [[ $hdfs_user == "" ]]
then
	printf "Missing argument: -u hdfs_user.\n\n"
	printf "$usage"
	exit 1
fi

#clean old logs
rm -f mount-tables.log

printf "Mounting tables...\t"

#run mount-tables-queries.sql
hive cli -v -hiveconf hdfs_user=$hdfs_user -f mount-tables-queries.sql >> mount-tables.log 2>&1
ret_code=$?

#check return status
if [[ $ret_code -eq 0 ]]
then
	#ok
	printf "[$(tput setaf 2)OK$(tput sgr 0)]\n\n"
else
	#a problem occurred
	printf "[$(tput setaf 1)FAILED$(tput sgr 0)]\n\n"
	printf "Check the logfile (mount-tables.log) and correct the issues.\n\n"
	exit 1
fi 