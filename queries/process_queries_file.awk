# Copyright Mattia Bertorello, Nicolò Bidotti 2014
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU  Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU  Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BEGIN								{
										group_num=0;
										query_num=0;
										prevstate=0;
										state=0;
										rule="";
									}
									{ 
										prevstate=state;
										state=0;
										rule="";
									}
/-- # .*/							{
										state=1;
										rule=rule "0";
									}
/-- Raw data -*/					{	
										type="raw";
										dir=0;
										state=1;
										rule=rule "1";
									}
/-- Bucketed by tens -*/			{
										type="bucketed_10";
										dir=0;
										state=1;
										rule=rule "2";
									}
/-- (.*)/							{
										if (state<1) {
											if (name!="" && group=="") {
												group=name;
												dir=0;
											}
											line=$0;
											line=substr(line,4);
											line=tolower(line);
											gsub(" ","_",line);
											gsub("[:|\"|(|)]","",line);
											name=line;
											state=2;
											rule=rule "3";
										}
									}
/CREATE|DROP|ALTER|INSERT|SELECT/,/.*;/	{
										if (dir==0) {
											if (group!="") cmd_mkdir="mkdir -p " "queries/" type "/g" sprintf("%03d", group_num) "_" group " 2>/dev/null"
												else cmd_mkdir="mkdir -p " "queries/" type " 2>/dev/null"
											system(cmd_mkdir);
											close(cmd_mkdir);
											dir=1;
										}
										if (group!="") file="queries/" type "/g" sprintf("%03d", group_num) "_" group "/q" sprintf("%03d", query_num) "_" name ".sql"
											else file="queries/" type "/g" sprintf("%03d", group_num) "_q" sprintf("%03d", query_num) "_" name ".sql";
										print > file;
										state=3;
										rule=rule "4";
									}
/;$/								{
										query_num++;
										state=4;
										rule=rule "5";
									}
/^$/					{
										group="";
										name="";
										if (prevstate == 4) group_num++;
										query_num=0;
										state=5;
										rule=rule "6";
									}
#{ print "\nline=" NR " text=\"" $0 "\""; print "rules=" rule; print "type=" type " group=" group " name=" name; }