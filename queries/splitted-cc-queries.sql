-- Copyright Mattia Bertorello, Nicolò Bidotti 2014

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU  Lesser General Public License as published
-- by the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.

-- You should have received a copy of the GNU  Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.


-- Clustering coefficient of entity graph
-- Create tables
DROP TABLE IF EXISTS entities_neighbours;
CREATE TABLE entities_neighbours
  STORED AS parquet LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/entitiesNeighbours'
  AS SELECT DISTINCT t.entity_id, t.neighbour
    FROM (
      SELECT from_entity_id AS entity_id, to_entity_id AS neighbour
        FROM complete_graph
      UNION ALL
      SELECT to_entity_id AS entity_id, from_entity_id AS neighbour
        FROM complete_graph
    ) t
    WHERE t.entity_id != t.neighbour
    ORDER BY entity_id, neighbour ASC;
DROP TABLE IF EXISTS entities_neighbourhood_size;
CREATE TABLE entities_neighbourhood_size
  STORED AS parquet LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/entitiesNeighbourhoodSize'
  AS SELECT entity_id, cast(count(DISTINCT neighbour) AS INT) AS neighbourhood_size
    FROM entities_neighbours
    GROUP BY entity_id
    ORDER BY entity_id ASC;
-- Calculate clustering coefficient
DROP TABLE IF EXISTS entities_clustering_coefficient;
CREATE TABLE entities_clustering_coefficient
  (entity_id INT, clustering_coefficient DOUBLE)
  STORED AS parquet LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/entitiesClusteringCoefficient';
INSERT INTO TABLE entities_clustering_coefficient
  SELECT
    t.entity_id,
    cast(t.num_triangles AS DOUBLE) / cast(((u.neighbourhood_size) * (u.neighbourhood_size - 1)) AS DOUBLE) AS clustering_coefficient
  FROM
    (
      SELECT n1.entity_id, count(*) as num_triangles
        FROM
          (SELECT *
             FROM entities_neighbours
               WHERE entity_id > ${lower_entity_id} AND entity_id <= ${upper_entity_id}) n1
          JOIN entities_neighbours n2 ON n1.neighbour = n2.entity_id
          JOIN entities_neighbours n3 ON n2.neighbour = n3.neighbour AND n1.entity_id = n3.entity_id
        GROUP BY n1.entity_id
    ) t
    JOIN entities_neighbourhood_size u ON (t.entity_id = u.entity_id);
SELECT total_clustering_coefficient/num_entities AS average_clustering_coefficient
  FROM
   (SELECT sum(clustering_coefficient) AS total_clustering_coefficient FROM entities_clustering_coefficient) t
   CROSS JOIN (SELECT count(distinct entity_id) AS num_entities FROM entities_neighbours) u;