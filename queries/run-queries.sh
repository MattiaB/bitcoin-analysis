#!/bin/bash
# Copyright Mattia Bertorello, Nicolò Bidotti 2014
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU  Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU  Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

usage="Usage: ./run-queries.sh [-h] -e (hive|impala) -f queries_file [-g start_group]
\t\t[-u hdfs_user]

Where:
	-h               \t print this help and exit.
	-e (hive|impala) \t the query engine to use. valid values: \"hive\" or \"impala\"
	-f queries_file  \t a text file containing the queries to run.
	-g start_group   \t the group to start from, 3 digits with leading zeroes.
	-u hdfs_user     \t used for table paths; tables will be saved in 
	                 \t hdfs:///user/hdfs_user/bitcoin-analysis/ .
	                 \t Default is \"admin\". Hive only!
\n"

#get arguments and check them
while test $# -gt 0
do
	case "$1" in
		-h)
			printf "$usage"
			exit 0
			;;
		-e)
			shift
			query_engine=$1
			shift
			;;
		-f)
			shift
			queries_file=$1
			shift
			;;
		-g)
			shift
			start_group=$1
			shift
			;;
		-u)
			shift
			hdfs_user=$1
			shift
			;;
		*)
			break
			;;
	esac
done
if [[ $query_engine == "" ]]
then
	printf "Missing argument: -e query_engine.\n\n"
	printf "$usage"
	exit 1
fi
if [[ $query_engine != "hive" && $query_engine != "impala" ]]
then
	printf "Invalid value \"$query_engine\" for argument: -e query_engine: must be \"hive\" or \"impala\".\n\n"
	printf "$usage"
	exit 1
fi
if [[ $queries_file == "" ]]
then
	printf "Missing argument: -f queries_file.\n\n"
	printf "$usage"
	exit 1
fi
if [[ ! -f $queries_file ]]
then
	printf "Invalid value \"$queries_file\" for argument: -f queries_file: not a file.\n\n"
	printf "$usage"
	exit 1
fi
if [[ $start_group == "" ]]
then
	start_group="000";
fi
if [[ $start_group != $(echo $start_group | grep -o -E [0-9][0-9][0-9]) ]]
then
	printf "Invalid value \"$start_group\" for argument: -g start_group: malformed.\n\n"
	printf "$usage"
	exit 1
fi
if [[ $hdfs_user == "" ]]
then
	hdfs_user="admin";
fi

#clean old logs/queries and check if results already exist
rm -f run-queries.log
rm -rf ./queries
if [[ -d "./results" ]]
then
	printf "Output directory already exists. Exiting...\n\n"
	exit 1
fi

#split analysis queries file
awk -f process_queries_file.awk $queries_file

#get groups
groups=$(find ./queries | grep -o -E g[0-9][0-9][0-9] | sort | uniq)

#run each group
for group in $groups
do
	group_num=${group/g/}
	
	#skip group if needed
	if [[ $group_num < $start_group ]]
	then
		continue
	fi
	
	#print group number and directory
	group_filedir=$(find ./queries | grep -E g$group_num | head -n1)
	if [ -d $group_filedir ]
	then
		group_dir=$group_filedir
	else
		group_dir=$(dirname $group_filedir)
	fi
	
	printf "Group: %s - %s\n" $group_num $group_dir
	
	#get a list of query file names in the group
	queries=$(find ./queries -wholename "*$group*.sql" | sort)
	
	#run each query
	for query in $queries
	do
		query_num=$(echo "$query" | grep -o -E q[0-9][0-9][0-9])
		query_num=${query_num/q/}
		
		#generate result file name and create directory
		result="${query/queries/results}"
		result="${result/sql/csv}"
		mkdir -p "$(dirname $result)"
		
		#run query
		while true
		do
			#print query number and (truncated) filename
			query_filename=$(basename $query)
			query_filename=${query_filename%%.*}
			query_fileextn=${query##*.}
			if [ ${#query_filename} -gt 40 ]
            then
                trunc_query_filename=$(echo "$query_filename" | cut -c1-39)
                query_pretty_name=${trunc_query_filename}...${query_fileextn}
            else
                query_pretty_name=${query_filename}.${query_fileextn}
			fi
			
			printf "\tQuery: %s - %-50s\t" $query_num $query_pretty_name
			
			printf "================================================================================\n" >> run-queries.log
			printf "Running query: $query\n" >> run-queries.log
			printf "================================================================================\n" >> run-queries.log
			
			#run specifying output file and format
			if [[ $query_engine == "hive" ]]
			then
				hive cli -i hive-init.sql -hiveconf hdfs_user=$hdfs_user -f $query 1>> $result 2>> run-queries.log
				ret_code=$?
			fi
			if [[ $query_engine == "impala" ]]
			then
				impala-shell --print_header --delimited --output_delimiter=',' -f $query -o $result >> run-queries.log 2>&1
				ret_code=$?
			fi
			
			printf "\n\n" >> run-queries.log
			
			#check return status
			if [[ $ret_code -eq 0 ]]
			then
				#ok, go on with the next query
				printf "[$(tput setaf 2)OK$(tput sgr 0)]\n"
				break
			else
				#a problem occurred, ask what to do
				printf "[$(tput setaf 1)FAILED$(tput sgr 0)]\n\n"
				
				#print link to problematic query file and log
				printf "Problematic query file:\n"
				printf "file://%s\n" $(readlink -f $query)
				printf "Check log file for details. Log file:\n"
				printf "file://%s\n\n" $(readlink -f ./run-queries.log)
				
				#retry if the user wants to
				retry=-1
				until [[ $retry -ge 0 ]]
				do
					printf "Retry? [Y/n/q]: "
					read input
					if [[ $input == "" || $input == "Y" || $input == "y" || $input == "Yes" || $input == "YES" || $input == "yes" ]]
					then
						#user wants to retry
						retry=1
						printf "Retrying query...\n\n"
					elif [[ $input == "N" || $input == "n" || $input == "No" || $input == "NO" || $input == "no" ]]
					then 
						#user wants to skip
						retry=0
						printf "Skipping query...\n\n"
					elif [[ $input == "Q" || $input == "q" || $input == "Quit" || $input == "QUIT" || $input == "quit" ]]
					then 
						#user wants to quit
						printf "Exiting...\n\n"
						exit 1
					else
						#user gave an invalid answer
						printf "Invalid answer!\n"
					fi
				done
				
				if [[ $retry -eq 1 ]]
				then
					continue
				fi
				if [[ $retry -eq 0 ]]
				then
					break
				fi
			fi
		done
	done
	printf "\n"
done 