#!/bin/bash
# Copyright Mattia Bertorello, Nicolò Bidotti 2014
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU  Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU  Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

usage="Usage: ./run-splitted-clustering-coefficient.sh [-h] [-q start_query]
/t/t[-s num_splits] [-r start_split] [-u hdfs_user]

Where:
	-h               \t print this help and exit.
	-q start_query   \t the query to start from, 3 digits with leading zeroes.
	-s num_splits    \t the number of splits to use. default is 20.
	-r start_split   \t resume from split number split_num.
	-u hdfs_user     \t used for table paths; tables will be saved in 
	                 \t hdfs:///user/hdfs_user/bitcoin-analysis/ .
	                 \t Default is \"admin\".
\n"

num_regex='^[0-9]+$'

#get arguments and check them
while test $# -gt 0
do
	case "$1" in
		-h)
			printf "$usage"
			exit 0
			;;
		-q)
			shift
			start_query=$1
			shift
			;;
		-s)
			shift
			declare -i num_splits=$1
			shift
			;;
		-n)
			shift
			declare -i start_split=$1
			shift
			;;
		-u)
			shift
			hdfs_user=$1
			shift
			;;
		*)
			break
			;;
	esac
done
if [[ $start_query == "" ]]
then
	start_group="000";
fi
if [[ $start_query != $(echo $start_query | grep -o -E [0-9][0-9][0-9]) ]]
then
	printf "Invalid value \"$start_query\" for argument: -q start_query: malformed.\n\n"
	printf "$usage"
	exit 1
fi
if [[ $num_splits == "" ]]
then
	declare -i num_splits=20;
fi
if ! [[ $num_splits =~ $num_regex ]]
then
   printf "Invalid value \"$num_splits\" for argument: -s num_splits: malformed.\n\n"
	printf "$usage"
	exit 1
fi
if [[ $start_split == "" ]]
then
	declare -i start_split=0;
fi
if ! [[ $start_split =~ $num_regex ]]
then
   printf "Invalid value \"$start_split\" for argument: -r start_split: malformed.\n\n"
	printf "$usage"
	exit 1
fi
if [[ $hdfs_user == "" ]]
then
	hdfs_user="admin";
fi

#clean old logs/queries and check if results already exist
rm -f run-splitted-cc.log
rm -rf queries-splitted-cc
if [[ -d "./results-splitted-cc" ]]
then
	printf "Output directory already exists. Exiting...\n\n"
	exit 1
fi

#split analysis queries file
mkdir queries-splitted-cc
cd queries-splitted-cc
awk -f ../process_queries_file.awk ../splitted-cc-queries.sql
cd ..

#get a list of query file names
queries=$(find ./queries-splitted-cc -wholename "*.sql" | sort)

#run each query
for query in $queries
do
	query_num=$(echo "$query" | grep -o -E q[0-9][0-9][0-9])
	query_num=${query_num/q/}
	
	#skip query if needed
	if [[ $query_num < $start_query ]]
	then
		continue
	fi
	
	#generate result file name and create directory
	result="${query//queries/results}"
	result="${result/sql/csv}"
	mkdir -p "$(dirname $result)"
	
	#get query number and (truncated) filename
	query_filename=$(basename $query)
	query_filename=${query_filename%%.*}
	query_fileextn=${query##*.}
	if [ ${#query_filename} -gt 40 ]
	then
		trunc_query_filename=$(echo "$query_filename" | cut -c1-39)
		query_pretty_name=${trunc_query_filename}...${query_fileextn}
	else
		query_pretty_name=${query_filename}.${query_fileextn}
	fi
	
	#run query
	if ! [[ $query_num == "006" ]]
	then
		while true
		do
			printf "Query: %s - %-50s\t" $query_num $query_pretty_name
			
			printf "================================================================================\n" >> run-splitted-cc.log
			printf "Running query: $query\n" >> run-splitted-cc.log
			printf "================================================================================\n" >> run-splitted-cc.log
			
			#run specifying output file and format
			hive cli -i hive-init.sql -hiveconf hdfs_user=$hdfs_user -f $query 1>> $result 2>> run-splitted-cc.log
			ret_code=$?
			
			#check return status
			if [[ $ret_code -eq 0 ]]
			then
				#ok, go on with the next query
				printf "[$(tput setaf 2)OK$(tput sgr 0)]\n"
				break
			else
				#a problem occurred, ask what to do
				printf "[$(tput setaf 1)FAILED$(tput sgr 0)]\n\n"
				
				#print link to problematic query file
				printf "Problematic query file:\n"
				printf "file://%s\n\n" $(readlink -f $query)
				printf "Check log file for details. Log file:\n"
				printf "file://%s\n\n" $(readlink -f ./run-splitted-cc.log)
				
				#retry if the user wants to
				retry=-1
				until [[ $retry -ge 0 ]]
				do
					printf "Retry? [Y/n/q]: "
					read input
					if [[ $input == "" || $input == "Y" || $input == "y" || $input == "Yes" || $input == "YES" || $input == "yes" ]]
					then
						#user wants to retry
						retry=1
						printf "Retrying query...\n\n"
					elif [[ $input == "N" || $input == "n" || $input == "No" || $input == "NO" || $input == "no" ]]
					then 
						#user wants to skip
						retry=0
						printf "Skipping query...\n\n"
					elif [[ $input == "Q" || $input == "q" || $input == "Quit" || $input == "QUIT" || $input == "quit" ]]
					then 
						#user wants to quit
						printf "Exiting...\n\n"
						exit 1
					else
						#user gave an invalid answer
						printf "Invalid answer!\n"
					fi
				done
				
				if [[ $retry -eq 1 ]]
				then
					continue
				fi
				if [[ $retry -eq 0 ]]
				then
					break
				fi
			fi
			
			printf "\n\n" >> run-splitted-cc.log
		done
	else
		printf "Query: %s - %-50s [Running in %s splits]\n" $query_num $query_pretty_name $num_splits
		
		printf "================================================================================\n" >> run-splitted-cc.log
		printf "Running query in splits: $query\n" >> run-splitted-cc.log
		printf "================================================================================\n" >> run-splitted-cc.log
		
		declare -i max_entity_id=0
		declare -i lower_entity_id=0
		declare -i upper_entity_id=0
		for ((split=0; split<=num_splits; split++))
		do
			#calculate lower and upper bounds for split
			if [[ $split -eq 1 ]]
			then
				lower_entity_id=-1
			else
				lower_entity_id=upper_entity_id
			fi
			upper_entity_id=$((max_entity_id/num_splits*split))
			
			#skip split if needed
			if [[ $split -lt $start_split ]]
			then
				continue
			fi				
			
			while true
			do
				if [[ $split -eq 0 ]]
				then
					printf "\tPreparing: finding maximum entity id %-26s\t"
					max_entity_id=$(hive cli -S -e "select max(entity_id) from entities_neighbours" 2>> /dev/null)
					ret_code=$?
				else
					printf "\tSplit: %3s/%-10s start id: %-10s end id: %-10s\t" $split $num_splits $lower_entity_id $upper_entity_id
					
					printf -- "--------------------------------------------------------------------------------\n" >> run-splitted-cc.log
					printf "Running split: %s/%s - start id: %s, end id: %s\t" $split $num_splits $lower_entity_id $upper_entity_id >> run-splitted-cc.log
					printf -- "--------------------------------------------------------------------------------\n" >> run-splitted-cc.log
		
					hive cli -i hive-init.sql -hivevar lower_entity_id=$lower_entity_id -hivevar upper_entity_id=$upper_entity_id -hiveconf hdfs_user=$hdfs_user -f $query 1>> /dev/null 2>> run-splitted-cc.log
					ret_code=$?
				fi
				
				#check return status
				if [[ $ret_code -eq 0 ]]
				then
					#ok, go on with the next split
					printf "[$(tput setaf 2)OK$(tput sgr 0)]\n"
					break
				else
					#a problem occurred, ask what to do
					printf "[$(tput setaf 1)FAILED$(tput sgr 0)]\n\n"
					
					#print info on split and link to log
					printf "Problematic split: %s\n" $split
					printf "Check log file for details. Log file:\n"
					printf "file://%s\n\n" $(readlink -f ./run-splitted-cc.log)
					
					#retry if the user wants to
					retry=-1
					until [[ $retry -ge 0 ]]
					do
						printf "Retry? [Y/n/q]: "
						read input
						if [[ $input == "" || $input == "Y" || $input == "y" || $input == "Yes" || $input == "YES" || $input == "yes" ]]
						then
							#user wants to retry
							retry=1
							printf "Retrying split...\n\n"
						elif [[ $input == "N" || $input == "n" || $input == "No" || $input == "NO" || $input == "no" ]]
						then 
							#user wants to skip
							retry=0
							printf "Skipping split...\n\n"
						elif [[ $input == "Q" || $input == "q" || $input == "Quit" || $input == "QUIT" || $input == "quit" ]]
						then 
							#user wants to quit
							printf "Exiting...\n\n"
							exit 1
						else
							#user gave an invalid answer
							printf "Invalid answer!\n"
						fi
					done
					
					if [[ $retry -eq 1 ]]
					then
						continue
					fi
					if [[ $retry -eq 0 ]]
					then
						break
					fi
				fi
				
				printf "\n\n" >> run-splitted-cc.log
			done
		done
	fi
done
printf "\n" 