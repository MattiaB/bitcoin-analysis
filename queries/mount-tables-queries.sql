-- Copyright Mattia Bertorello, Nicolò Bidotti 2014

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU  Lesser General Public License as published
-- by the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.

-- You should have received a copy of the GNU  Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

DROP TABLE IF EXISTS transaction_inputs;
CREATE EXTERNAL TABLE transaction_inputs
(block_height BIGINT, transaction_hash STRING, transaction_id BIGINT,
 time BIGINT, input_transaction_hash STRING, output_index BIGINT)
STORED AS parquet
LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/transactionInputs/';

DROP TABLE IF EXISTS transaction_outputs;
CREATE EXTERNAL TABLE transaction_outputs
(block_height BIGINT, transaction_hash STRING, transaction_id BIGINT,
 time BIGINT, address_hash STRING, output_index BIGINT, satoshi BIGINT)
STORED AS parquet
LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/transactionOutputs/';

DROP TABLE IF EXISTS blocks;
CREATE EXTERNAL TABLE blocks
(block_hash STRING, prevBlockHash STRING,
 merkleRoot STRING, height BIGINT, time BIGINT,
 difficulty BIGINT, work BIGINT)
STORED AS parquet
LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/blocks/';

DROP TABLE IF EXISTS complete_transaction_inputs;
CREATE EXTERNAL TABLE complete_transaction_inputs
(transaction_hash STRING, transaction_id INT, block_time BIGINT,
input_transaction_hash STRING, input_transaction_id INT, output_index INT,
address_hash STRING, address_id INT, satoshis BIGINT)
STORED AS parquet
LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/completeTransactionInputs';

DROP TABLE IF EXISTS complete_transaction_outputs;
CREATE EXTERNAL TABLE complete_transaction_outputs
(transaction_hash STRING, transaction_id INT, block_time BIGINT,
address_hash STRING, address_id INT, output_index INT, satoshis BIGINT)
STORED AS parquet
LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/completeTransactionOutputs';

DROP TABLE IF EXISTS entities_edges;
CREATE EXTERNAL TABLE entities_edges
(from_address_id INT, to_address_id INT)
STORED AS parquet
LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/entitiesEdges';

DROP TABLE IF EXISTS transaction_edges;
CREATE EXTERNAL TABLE transaction_edges
(transaction_id INT, time BIGINT, from_address_id INT, to_address_id INT,
satoshis BIGINT)
STORED AS parquet
LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/transactionEdges';

DROP TABLE IF EXISTS addresses;
CREATE EXTERNAL TABLE addresses
(id INT, hash STRING)
STORED as parquet
LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/addresses';

DROP TABLE IF EXISTS entities;
CREATE EXTERNAL TABLE entities
(address_id INT, entity_id INT)
STORED AS rcfile LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/entities/';

DROP TABLE IF EXISTS entities_parquet;
CREATE EXTERNAL TABLE entities_parquet
(address_id INT, entity_id INT)
STORED as parquet
LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/entitiesParquet';

DROP TABLE IF EXISTS btc_to_usd;
CREATE EXTERNAL TABLE btc_to_usd(day bigint, exchange_rate double)
   ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' LINES TERMINATED BY '\n'
   STORED AS TEXTFILE LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/btcToUsd';

DROP TABLE IF EXISTS complete_graph;
CREATE EXTERNAL TABLE complete_graph
(from_entity_id INT, to_entity_id INT, dollars DOUBLE, satoshis BIGINT)
STORED AS parquet
LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/completeGraph';

DROP TABLE IF EXISTS transaction_edges_entities_dollars;
CREATE EXTERNAL TABLE transaction_edges_entities_dollars
(block_height BIGINT, transaction_id INT, from_entity_id INT, to_entity_id INT, time BIGINT, 
output_index INT, satoshis BIGINT, dollars DOUBLE)
STORED AS parquet
LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/transactionEdgesEntitiesDollars';

DROP TABLE IF EXISTS entities_degree;
CREATE EXTERNAL TABLE entities_degree
(entity_id INT, in_degree INT, out_degree INT,
in_neighbourhood_in_degree INT, in_neighbourhood_out_degree INT,
out_neighbourhood_in_degree INT, out_neighbourhood_out_degree INT)
STORED AS rcfile LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/entitiesDegree';

DROP TABLE IF EXISTS entities_page_rank;
CREATE EXTERNAL TABLE entities_page_rank
(entity_id INT, rank DOUBLE)
STORED AS rcfile LOCATION '/user/${hiveconf:hdfs_user}/bitcoin-analysis/entitiesPageRank';