-- Copyright Mattia Bertorello, Nicolò Bidotti 2014

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU  Lesser General Public License as published
-- by the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.

-- You should have received a copy of the GNU  Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-- # Graph analysis queries # --------------------------------------------------



-- # Generic grah statistics # -------------------------------------------------

-- Raw data --------------------------------------------------------------------

-- Number of vertices: total number of entities
SELECT count(*) AS num_vertices
  FROM entities_degree;

-- Number of edges: total number of connections between entities
SELECT count(*) AS num_edges
  FROM complete_graph;

-- Number of transactions between entities (multigraph with loops)
SELECT count(*) AS num_transaction_edges
  FROM transaction_edges_entities_dollars;

-- Number of transactions between entities (multigraph without loops)
SELECT count(*) AS num_transaction_edges_no_loops
  FROM transaction_edges_entities_dollars
  WHERE from_entity_id != to_entity_id;

-- Number of transactions inside entities (loops)
SELECT count(*) AS num_loops_in_transaction_edges
  FROM transaction_edges_entities_dollars
  WHERE from_entity_id = to_entity_id;

-- Graph density
SELECT
                           cast(num_edges AS DOUBLE)
                                        /
    cast((cast(num_vertices AS BIGINT) * cast((num_vertices - 1) AS BIGINT)) AS DOUBLE) AS density
  FROM
    (SELECT count(*) AS num_edges
       FROM complete_graph) AS t1
    CROSS JOIN
    (SELECT count(*) AS num_vertices
       FROM entities_degree) AS t2;



-- # Degree distributions # ----------------------------------------------------

-- Raw data --------------------------------------------------------------------

-- Degree distribution of entites
-- In-degree
SELECT in_degree, count(*) AS num_entities
  FROM entities_degree
  GROUP BY in_degree
  ORDER BY in_degree ASC;
-- Out-degree
SELECT out_degree, count(*) AS num_entities
  FROM entities_degree
  GROUP BY out_degree
  ORDER BY out_degree ASC;
-- Degree
SELECT (in_degree + out_degree) AS degree, count(*) AS num_entities
  FROM entities_degree
  GROUP BY degree
  ORDER BY degree ASC;
-- Average in, out and "undirected" degree
SELECT avg(in_degree) AS avg_in_degree, avg(out_degree) AS avg_out_degree, avg(in_degree + out_degree) AS avg_degree
  FROM entities_degree;

-- Degree distribution of in-neighbourhoods
-- In-degree
SELECT in_neighbourhood_in_degree, count(*) AS num_entities
  FROM entities_degree
  GROUP BY in_neighbourhood_in_degree
  ORDER BY in_neighbourhood_in_degree ASC;
-- Out-degree
SELECT in_neighbourhood_out_degree, count(*) AS num_entities
  FROM entities_degree
  GROUP BY in_neighbourhood_out_degree
  ORDER BY in_neighbourhood_out_degree ASC;
-- Degree
SELECT (in_neighbourhood_in_degree + in_neighbourhood_out_degree) AS in_neighbourhood_degree, count(*) AS num_entities
  FROM entities_degree
  GROUP BY in_neighbourhood_degree
  ORDER BY in_neighbourhood_degree ASC;
-- Average in, out and "undirected" degree
SELECT
    avg(in_neighbourhood_in_degree) AS avg_in_neighbourhood_in_degree,
    avg(in_neighbourhood_out_degree) AS avg_in_neighbourhood_out_degree,
    avg(in_neighbourhood_in_degree + in_neighbourhood_out_degree) AS avg_in_neighbourhood_degree
  FROM entities_degree;

-- Degree distribution of out-neighbourhoods
-- In-degree
SELECT out_neighbourhood_in_degree, count(*) AS num_entities
  FROM entities_degree
  GROUP BY out_neighbourhood_in_degree
  ORDER BY out_neighbourhood_in_degree ASC;
-- Out-degree
SELECT out_neighbourhood_out_degree, count(*) AS num_entities
  FROM entities_degree
  GROUP BY out_neighbourhood_out_degree
  ORDER BY out_neighbourhood_out_degree ASC;
-- Degree
SELECT (out_neighbourhood_in_degree + out_neighbourhood_out_degree) AS out_neighbourhood_degree, count(*) AS num_entities
  FROM entities_degree
  GROUP BY out_neighbourhood_degree
  ORDER BY out_neighbourhood_degree ASC;
-- Average in, out and "undirected" degree
SELECT
    avg(out_neighbourhood_in_degree) AS avg_out_neighbourhood_in_degree,
    avg(out_neighbourhood_out_degree) AS avg_out_neighbourhood_out_degree,
    avg(out_neighbourhood_in_degree + out_neighbourhood_out_degree) AS avg_out_neighbourhood_degree
  FROM entities_degree;

-- Entities degree vs neighbourhood degree
-- In-degree vs in-neighbourhood in-degree
SELECT in_degree, avg(in_neighbourhood_in_degree) AS in_neighbourhood_in_degree
  FROM entities_degree
  GROUP BY in_degree
  ORDER BY in_degree ASC;
-- Out-degree vs out-neighbourhood out-degree
SELECT out_degree, avg(out_neighbourhood_out_degree) AS out_neighbourhood_out_degree
  FROM entities_degree
  GROUP BY out_degree
  ORDER BY out_degree ASC;
-- Degree vs neighbourhood degree
SELECT
    (in_degree + out_degree) AS degree,
    avg(in_neighbourhood_in_degree + in_neighbourhood_out_degree +
	    out_neighbourhood_in_degree + out_neighbourhood_out_degree) AS neighbourhood_degree
  FROM entities_degree
  GROUP BY degree
  ORDER BY degree ASC;

-- Bucketed by tens ------------------------------------------------------------

-- Degree distribution of entites
-- In-degree
SELECT
    floor(in_degree/10) AS bucket_value,
	count(*) AS num_entities,
	concat(cast(min(in_degree) AS STRING),
	       ' - ',
		   cast(max(in_degree) AS STRING)) AS in_degree_range
  FROM entities_degree
  GROUP BY bucket_value
  ORDER BY bucket_value ASC;
-- Out-degree
SELECT
    floor(out_degree/10) AS bucket_value,
	count(*) AS num_entities,
	concat(cast(min(out_degree) AS STRING),
	       ' - ',
	       cast(max(out_degree) AS STRING)) AS out_degree_range
  FROM entities_degree
  GROUP BY bucket_value
  ORDER BY bucket_value ASC;
-- Degree
SELECT
    floor((in_degree + out_degree)/10) AS bucket_value,
	count(*) AS num_entities,
	concat(cast(min(in_degree + out_degree) AS STRING),
	       ' - ',
		   cast(max(in_degree + out_degree) AS STRING)) AS degree_range
  FROM entities_degree
  GROUP BY bucket_value
  ORDER BY bucket_value ASC;

-- Degree distribution of in-neighbourhoods
-- In-degree
SELECT
    floor(in_neighbourhood_in_degree/10) AS bucket_value,
	count(*) AS num_entities,
    concat(cast(min(in_neighbourhood_in_degree) AS STRING),
	       ' - ',
		   cast(max(in_neighbourhood_in_degree) AS STRING)) AS in_neighbourhood_in_degree_range
  FROM entities_degree
  GROUP BY bucket_value
  ORDER BY bucket_value ASC;
-- Out-degree
SELECT
    floor(in_neighbourhood_out_degree/10) AS bucket_value,
	count(*) AS num_entities,
    concat(cast(min(in_neighbourhood_out_degree) AS STRING),
	       ' - ',
		   cast(max(in_neighbourhood_out_degree) AS STRING)) AS in_neighbourhood_out_degree_range
  FROM entities_degree
  GROUP BY bucket_value
  ORDER BY bucket_value ASC;
-- Degree
SELECT
    floor((in_neighbourhood_in_degree + in_neighbourhood_out_degree)/10) AS bucket_value,
	count(*) AS num_entities,
    concat(cast(min(in_neighbourhood_in_degree + in_neighbourhood_out_degree) AS STRING),
	       ' - ',
		   cast(max(in_neighbourhood_in_degree + in_neighbourhood_out_degree) AS STRING)) AS in_neighbourhood_degree_range
  FROM entities_degree
  GROUP BY bucket_value
  ORDER BY bucket_value ASC;

-- Degree distribution of out-neighbourhoods
-- In-degree
SELECT
    floor(out_neighbourhood_in_degree/10) AS bucket_value,
	count(*) AS num_entities,
    concat(cast(min(out_neighbourhood_in_degree) AS STRING),
	       ' - ',
		   cast(max(out_neighbourhood_in_degree) AS STRING)) AS out_neighbourhood_in_degree_range
  FROM entities_degree
  GROUP BY bucket_value
  ORDER BY bucket_value ASC;
-- Out-degree
SELECT
    floor(out_neighbourhood_out_degree/10) AS bucket_value,
	count(*) AS num_entities,
    concat(cast(min(out_neighbourhood_out_degree) AS STRING),
	       ' - ',
		   cast(max(out_neighbourhood_out_degree) AS STRING)) AS out_neighbourhood_out_degree_range
  FROM entities_degree
  GROUP BY bucket_value
  ORDER BY bucket_value ASC;
-- Degree
SELECT
    floor((out_neighbourhood_in_degree + out_neighbourhood_out_degree)/10) AS bucket_value,
	count(*) AS num_entities,
    concat(cast(min(out_neighbourhood_in_degree + out_neighbourhood_out_degree) AS STRING),
	       ' - ',
		   cast(max(out_neighbourhood_in_degree + out_neighbourhood_out_degree) AS STRING)) AS out_neighbourhood_degree_range
  FROM entities_degree
  GROUP BY bucket_value
  ORDER BY bucket_value ASC;

-- Entities degree vs neighbourhood degree
-- In-degree vs in-neighbourhood in-degree
SELECT
    floor(in_degree/10) AS bucket_value,
	avg(in_neighbourhood_in_degree) AS in_neighbourhood_in_degree,
	concat(cast(min(in_degree) AS STRING),
	       ' - ',
		   cast(max(in_degree) AS STRING)) AS in_degree_range
  FROM entities_degree
  GROUP BY bucket_value
  ORDER BY bucket_value ASC;
-- Out-degree vs out-neighbourhood out-degree
SELECT
    floor(out_degree/10) AS bucket_value,
	avg(out_neighbourhood_out_degree) AS out_neighbourhood_out_degree,
	concat(cast(min(out_degree) AS STRING),
	       ' - ',
	       cast(max(out_degree) AS STRING)) AS out_degree_range
  FROM entities_degree
  GROUP BY bucket_value
  ORDER BY bucket_value ASC;
-- Degree vs neighbourhood degree
SELECT
    floor((in_degree + out_degree)/10) AS bucket_value,
	avg(in_neighbourhood_in_degree + in_neighbourhood_out_degree +
	    out_neighbourhood_in_degree + out_neighbourhood_out_degree) AS neighbourhood_degree,
	concat(cast(min(in_degree + out_degree) AS STRING),
	       ' - ',
		   cast(max(in_degree + out_degree) AS STRING)) AS degree_range
  FROM entities_degree
  GROUP BY bucket_value
  ORDER BY bucket_value ASC;



-- # Other analyses # ----------------------------------------------------------

-- Raw data --------------------------------------------------------------------

-- In-out degrees vs total in-out money
-- In-degree vs total inputs
SELECT in_degree, avg(total_inputs_dollars), avg(total_inputs_satoshis)
  FROM (
    SELECT sum(dollars) AS total_inputs_dollars, sum(satoshis) AS total_inputs_satoshis, to_entity_id
    FROM complete_graph 
    GROUP BY to_entity_id
  ) AS tot JOIN entities_degree ON (to_entity_id = entity_id)
  GROUP BY in_degree
  ORDER BY in_degree ASC;
-- Out-degree vs total outputs
SELECT out_degree, avg(total_outputs_dollars), avg(total_outputs_satoshis)
  FROM (
    SELECT sum(dollars) AS total_outputs_dollars, sum(satoshis) AS total_outputs_satoshis, from_entity_id
    FROM complete_graph 
    GROUP BY from_entity_id
  ) AS tot JOIN entities_degree ON (from_entity_id = entity_id)
  GROUP BY out_degree
  ORDER BY out_degree ASC;

-- Distribution of number of addresses per entity
DROP VIEW IF EXISTS addresses_per_entity;
CREATE VIEW addresses_per_entity(entity_id, num_addresses) AS
  SELECT entity_id, count(*) AS num_addresses 
  FROM entities_parquet
  GROUP BY entity_id;
SELECT num_addresses, count(*) AS num_entities
  FROM addresses_per_entity
  GROUP BY num_addresses
  ORDER BY num_addresses ASC;

-- Distribution of number of transactions per entity
DROP VIEW IF EXISTS transactions_per_entity;
CREATE VIEW transactions_per_entity(entity_id, num_transactions) AS
  SELECT from_entity_id AS entity_id, (trans_from_entity + trans_to_entity) AS num_transactions 
  FROM
    (SELECT count(DISTINCT transaction_id) AS trans_from_entity, from_entity_id FROM transaction_edges_entities_dollars
    GROUP BY from_entity_id) AS t1
	JOIN
	(SELECT count(DISTINCT transaction_id) AS trans_to_entity, to_entity_id FROM transaction_edges_entities_dollars
    GROUP BY to_entity_id) AS t2
    ON (from_entity_id = to_entity_id);
SELECT num_transactions, count(*) AS num_entities
  FROM transactions_per_entity
  GROUP BY num_transactions
  ORDER BY num_transactions ASC;

-- Distribution of number of transactions per address
DROP VIEW IF EXISTS transactions_per_address;
CREATE VIEW transactions_per_address(address_id, num_transactions) AS
  SELECT from_address_id AS address_id, (trans_from_address + trans_to_address) AS num_transactions 
  FROM
    (SELECT count(DISTINCT transaction_id) AS trans_from_address, from_address_id FROM transaction_edges
    GROUP BY from_address_id) AS t1
	JOIN
	(SELECT count(DISTINCT transaction_id) AS trans_to_address, to_address_id FROM transaction_edges
    GROUP BY to_address_id) AS t2
    ON (from_address_id = to_address_id);
SELECT num_transactions, count(*) AS num_addresses
  FROM transactions_per_address
  GROUP BY num_transactions
  ORDER BY num_transactions ASC;

-- Bucketed by tens ------------------------------------------------------------

-- In-out degrees vs total in-out money
-- In-degree vs total inputs
SELECT
    floor(in_degree/10) AS bucket_value,
    avg(total_inputs_dollars) AS total_inputs_dollars,
	avg(total_inputs_satoshis) AS total_inputs_satoshis,
	concat(cast(min(in_degree) AS STRING),
	       ' - ',
		   cast(max(in_degree) AS STRING)) AS in_degree_range
  FROM (
    SELECT sum(dollars) AS total_inputs_dollars, sum(satoshis) AS total_inputs_satoshis, to_entity_id
    FROM complete_graph 
    GROUP BY to_entity_id
  ) AS tot JOIN entities_degree ON (to_entity_id = entity_id)
  GROUP BY bucket_value
  ORDER BY bucket_value ASC;
-- Out-degree vs total outputs
SELECT
    floor(out_degree/10) AS bucket_value,
    avg(total_outputs_dollars) AS total_outputs_dollars,
	avg(total_outputs_satoshis) AS total_outputs_satoshis,
	concat(cast(min(out_degree) AS STRING),
	       ' - ',
	       cast(max(out_degree) AS STRING)) AS out_degree_range
  FROM (
    SELECT sum(dollars) AS total_outputs_dollars, sum(satoshis) AS total_outputs_satoshis, from_entity_id
    FROM complete_graph 
    GROUP BY from_entity_id
  ) AS tot JOIN entities_degree ON (from_entity_id = entity_id)
  GROUP BY bucket_value
  ORDER BY bucket_value ASC;

-- Distribution of number of addresses per entity
DROP VIEW IF EXISTS addresses_per_entity;
CREATE VIEW addresses_per_entity(entity_id, num_addresses) AS
  SELECT entity_id, count(*) AS num_addresses 
  FROM entities_parquet
  GROUP BY entity_id;
SELECT
    floor(num_addresses/10) AS bucket_value,
    count(*) AS num_entities,
	concat(cast(min(num_addresses) AS STRING),
	       ' - ',
	       cast(max(num_addresses) AS STRING)) AS num_addresses_range
  FROM addresses_per_entity
  GROUP BY bucket_value
  ORDER BY bucket_value ASC;

-- Distribution of number of transactions per entity
DROP VIEW IF EXISTS transactions_per_entity;
CREATE VIEW transactions_per_entity(entity_id, num_transactions) AS
  SELECT from_entity_id AS entity_id, (trans_from_entity + trans_to_entity) AS num_transactions 
  FROM
    (SELECT count(DISTINCT transaction_id) AS trans_from_entity, from_entity_id FROM transaction_edges_entities_dollars
    GROUP BY from_entity_id) AS t1
	JOIN
	(SELECT count(DISTINCT transaction_id) AS trans_to_entity, to_entity_id FROM transaction_edges_entities_dollars
    GROUP BY to_entity_id) AS t2
    ON (from_entity_id = to_entity_id);
SELECT
    floor(num_transactions/10) AS bucket_value,
    count(*) AS num_entities,
	concat(cast(min(num_transactions) AS STRING),
	       ' - ',
	       cast(max(num_transactions) AS STRING)) AS num_transactions_range
  FROM transactions_per_entity
  GROUP BY bucket_value
  ORDER BY bucket_value ASC;

-- Distribution of number of transactions per address
DROP VIEW IF EXISTS transactions_per_address;
CREATE VIEW transactions_per_address(address_id, num_transactions) AS
  SELECT from_address_id AS address_id, (trans_from_address + trans_to_address) AS num_transactions 
  FROM
    (SELECT count(DISTINCT transaction_id) AS trans_from_address, from_address_id FROM transaction_edges
    GROUP BY from_address_id) AS t1
	JOIN
	(SELECT count(DISTINCT transaction_id) AS trans_to_address, to_address_id FROM transaction_edges
    GROUP BY to_address_id) AS t2
    ON (from_address_id = to_address_id);
SELECT
    floor(num_transactions/10) AS bucket_value,
    count(*) AS num_addresses,
	concat(cast(min(num_transactions) AS STRING),
	       ' - ',
	       cast(max(num_transactions) AS STRING)) AS num_transactions_range
  FROM transactions_per_address
  GROUP BY bucket_value
  ORDER BY bucket_value ASC;



-- # Global graph statistics # -------------------------------------------------

-- Raw data --------------------------------------------------------------------

-- Assortativity of entity graph
-- Create assortativity constants table
DROP TABLE IF EXISTS assortativity_constants;
CREATE EXTERNAL TABLE assortativity_constants
  (e_inverse DOUBLE, j_in_mean DOUBLE, j_out_mean DOUBLE, k_in_mean DOUBLE, k_out_mean DOUBLE)
  STORED AS parquet LOCATION '/user/admin/bitcoin-analysis/assortativityConstants';
INSERT OVERWRITE TABLE assortativity_constants
  SELECT *
  FROM
    (SELECT (1/count(*)) AS e_inverse FROM complete_graph) AS t1 CROSS JOIN
    (SELECT avg(in_degree) AS j_in_mean, avg(out_degree) AS j_out_mean FROM complete_graph JOIN entities_degree ON (from_entity_id = entity_id)) AS t2 CROSS JOIN
    (SELECT avg(in_degree) AS k_in_mean, avg(out_degree) AS k_out_mean FROM complete_graph JOIN entities_degree ON (to_entity_id = entity_id)) AS t3;
-- r(in,in)
SELECT
    (  
	                       (e_inverse * sum((j.in_degree - j_in_mean)*(k.in_degree - k_in_mean)))  
                                                                   /  
     (sqrt(e_inverse * sum(pow((j.in_degree - j_in_mean), 2))) * sqrt(e_inverse * sum(pow((k.in_degree - k_in_mean), 2))))  
    ) AS r_in_in  
  FROM   
    complete_graph c  
    JOIN entities_degree j ON (c.from_entity_id = j.entity_id)  
    JOIN entities_degree k ON (c.to_entity_id = k.entity_id)  
    CROSS JOIN assortativity_constants  
  GROUP BY e_inverse;
-- r(in,out)
SELECT
    (  
	                       (e_inverse * sum((j.in_degree - j_in_mean)*(k.out_degree - k_out_mean)))  
                                                                   /  
     (sqrt(e_inverse * sum(pow((j.in_degree - j_in_mean), 2))) * sqrt(e_inverse * sum(pow((k.out_degree - k_out_mean), 2))))  
    ) AS r_in_out  
  FROM   
    complete_graph c  
    JOIN entities_degree j ON (c.from_entity_id = j.entity_id)  
    JOIN entities_degree k ON (c.to_entity_id = k.entity_id)  
    CROSS JOIN assortativity_constants  
  GROUP BY e_inverse;
-- r(out,in)
SELECT
    (  
	                       (e_inverse * sum((j.out_degree - j_out_mean)*(k.in_degree - k_in_mean)))  
                                                                   /  
     (sqrt(e_inverse * sum(pow((j.out_degree - j_out_mean), 2))) * sqrt(e_inverse * sum(pow((k.in_degree - k_in_mean), 2))))  
    ) AS r_out_in  
  FROM   
    complete_graph c  
    JOIN entities_degree j ON (c.from_entity_id = j.entity_id)  
    JOIN entities_degree k ON (c.to_entity_id = k.entity_id)  
    CROSS JOIN assortativity_constants  
  GROUP BY e_inverse;
-- r(out,out)
SELECT
    (  
	                       (e_inverse * sum((j.out_degree - j_out_mean)*(k.out_degree - k_out_mean)))  
                                                                   /  
     (sqrt(e_inverse * sum(pow((j.out_degree - j_out_mean), 2))) * sqrt(e_inverse * sum(pow((k.out_degree - k_out_mean), 2))))  
    ) AS r_out_out  
  FROM   
    complete_graph c  
    JOIN entities_degree j ON (c.from_entity_id = j.entity_id)  
    JOIN entities_degree k ON (c.to_entity_id = k.entity_id)  
    CROSS JOIN assortativity_constants  
  GROUP BY e_inverse;