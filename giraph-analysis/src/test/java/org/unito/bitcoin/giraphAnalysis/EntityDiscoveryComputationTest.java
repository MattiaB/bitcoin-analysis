/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.unito.bitcoin.giraphAnalysis;

import com.google.common.collect.Iterables;
import org.apache.giraph.conf.GiraphConfiguration;
import org.apache.giraph.io.formats.IdWithValueTextOutputFormat;
import org.apache.giraph.io.formats.IntIntNullTextInputFormat;
import org.apache.giraph.utils.InternalVertexRunner;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by mattia on 10/14/14.
 */
public class EntityDiscoveryComputationTest {
    /*
        All graph must be undirected
     */
    @Test
    public void testSimple1() throws Exception {

        // A small graph with two component
        String[] graph = new String[] {
                "1 4 2",
                "2 4",
                "3 5",
                "4 1 2",
                "5 3"
        };
        String [] resultVertex = new String[] {
                "1	1",
                "2	1",
                "3	3",
                "4	1",
                "5	3",
        };
        Iterable<String> results = compute(graph);
        assertNotNull(results);
        assertEquals(convertToMap(resultVertex), convertToMap(results));
    }
    @Test
    public void testSimple2() throws Exception {

        // A chain graph
        String[] graph = new String[] {
                "1 2",
                "2 1 3",
                "3 2 4",
                "4 3 5",
                "5 4 6",
                "6 5 7",
                "7 6"
        };
        String [] resultVertex = new String[] {
                "1	1",
                "2	1",
                "3	1",
                "4	1",
                "5	1",
                "6	1",
                "7	1",
        };

        Iterable<String> results = compute(graph);
        assertNotNull(results);
        assertEquals(convertToMap(resultVertex), convertToMap(results));
    }
    @Test
    public void testSimple3() throws Exception {

        // A small graph
        String[] graph = new String[] {
                "1 2 3 4 5 6 7 8 9 10",
                "2 10 1",
                "3 10 1",
                "4 10 1",
                "5 10 1",
                "6 10 1",
                "7 10 1",
                "8 10 1",
                "9 10 1",
                "10 1 2 3 4 5 6 7 8 9",
        };
        String [] resultVertex = new String[] {
                "1	1",
                "2	1",
                "3	1",
                "4	1",
                "5	1",
                "6	1",
                "7	1",
                "8	1",
                "9	1",
                "10	1",
        };

        Iterable<String> results = compute(graph);
        assertNotNull(results);
        assertEquals(convertToMap(resultVertex), convertToMap(results));
    }
    private Iterable<String> compute(String[] graph, int max_message) throws Exception {
        GiraphConfiguration conf = new GiraphConfiguration();
        EntityDiscoveryComputation.MESSAGE_LIMIT.set(conf, max_message);
        conf.setComputationClass(EntityDiscoveryComputation.class);
        conf.setVertexInputFormatClass(IntIntNullTextInputFormat.class);
        conf.setVertexOutputFormatClass(
                IdWithValueTextOutputFormat.class);
        // Run internally
        Iterable<String> result = InternalVertexRunner.run(conf, graph);
        return result;

    }
    private Iterable<String> compute(String[] graph) throws Exception {
      return compute(graph, 2);
    }
    private Map<Integer, Integer> convertToMap(Iterable<String> stringIterable) {
        return convertToMap(Iterables.toArray(stringIterable, String.class));
    }
    private Map<Integer, Integer> convertToMap(String[] stringIterable) {
        Map<Integer, Integer> result = new HashMap<>();
        for (String elem : stringIterable) {
            String[] vertexAndValue = elem.split("\t");
            result.put(
                    Integer.parseInt(vertexAndValue[0]),
                    Integer.parseInt(vertexAndValue[1])
            );
        }
        return result;
    }

}
