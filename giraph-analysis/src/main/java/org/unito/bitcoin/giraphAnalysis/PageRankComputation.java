/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * Created from PageRankComputation and RandomWalkComputation from 
 * giraph-examples (HEAD 065d718d79d4ed6bab0b6a1f4e025cab73bf8a30)
 * Nicolò Bidotti, 06/2014
 */

package org.unito.bitcoin.giraphAnalysis;

import java.io.IOException;

import org.apache.giraph.edge.Edge;
import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.graph.Vertex;
import org.apache.giraph.utils.MathUtils;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;

/**
 * The PageRank algorithm, with uniform transition probabilities on the edges
 * http://en.wikipedia.org/wiki/PageRank
 * 
 * IMPORTANT: only out-edges must be present in the graph, otherwise both the
 * dangling vertex and transition probability calculations will be wrong!
 */
public class PageRankComputation
        extends
        BasicComputation<IntWritable, DoubleWritable, NullWritable, DoubleWritable> {
	/** Name of aggregator for the probability assigned to dangling vertices */
	static final String CUMULATIVE_DANGLING_PROBABILITY = PageRankComputation.class
	        .getName() + ".cumulativeDanglingProbability";
	/** Name of aggregator for the probability assigned to all vertices */
	static final String CUMULATIVE_PROBABILITY = PageRankComputation.class
	        .getName() + ".cumulativeProbability";
	/** Name of aggregator for the number of dangling vertices */
	static final String NUM_DANGLING_VERTICES = PageRankComputation.class
	        .getName() + ".numDanglingVertices";
	/**
	 * Name of aggregator for the L1 norm of the probability difference, used
	 * for convergence detection
	 */
	static final String L1_NORM_OF_PROBABILITY_DIFFERENCE = PageRankComputation.class
	        .getName() + ".l1NormOfProbabilityDifference";
	/** Maximum number of iterations */
	private static final int MAX_SUPERSTEPS = 30;
	/** Teleportation probability */
	private static final float TELEPORTATION_PROBABILITY = 0.15f;
	/** Reusable {@link DoubleWritable} instance to avoid object instantiation */
	private final DoubleWritable doubleWritable = new DoubleWritable();
	/** Reusable {@link IntWritable} for counting dangling vertices */
	private final IntWritable one = new IntWritable(1);
	
	/**
	 * Compute an initial probability value for the vertex. Per default, we
	 * start with a uniform distribution.
	 * 
	 * @return The initial probability value.
	 */
	protected double initialProbability() {
		return 1.0 / getTotalNumVertices();
	}
	
	/**
	 * Compute the probability of transitioning to a neighbor vertex
	 * 
	 * @param vertex
	 *            Vertex
	 * @param stateProbability
	 *            current steady state probability of the vertex
	 * @param edge
	 *            edge to neighbor
	 * @return the probability of transitioning to a neighbor vertex
	 */
	protected double transitionProbability(
	        Vertex<IntWritable, DoubleWritable, NullWritable> vertex,
	        double stateProbability, Edge<IntWritable, NullWritable> edge) {
		// Uniform transition probability
		return stateProbability / vertex.getNumEdges();
	}
	
	/**
	 * Returns the cumulative probability from dangling vertices.
	 * 
	 * @return The cumulative probability from dangling vertices.
	 */
	protected double getDanglingProbability() {
		return this.<DoubleWritable> getAggregatedValue(
		        CUMULATIVE_DANGLING_PROBABILITY).get();
	}
	
	/**
	 * Returns the cumulative probability from all vertices.
	 * 
	 * @return The cumulative probability from all vertices.
	 */
	protected double getPreviousCumulativeProbability() {
		return this.<DoubleWritable> getAggregatedValue(CUMULATIVE_PROBABILITY)
		        .get();
	}
	
	/**
	 * Perform a single step of a random walk computation.
	 * 
	 * @param vertex
	 *            Vertex
	 * @param partialRanks
	 *            Messages received in the previous step.
	 * @param teleportationProbability
	 *            Probability of teleporting to another vertex.
	 * @return The new probability value.
	 */
	protected double recompute(
	        Vertex<IntWritable, DoubleWritable, NullWritable> vertex,
	        Iterable<DoubleWritable> partialRanks,
	        double teleportationProbability) {
		// Rank contribution from incident neighbors
		double rankFromNeighbors = MathUtils.sum(partialRanks);
		// Rank contribution from dangling vertices
		double danglingContribution = getDanglingProbability()
		        / getTotalNumVertices();
		
		// Recompute rank
		return (1d - teleportationProbability)
		        * (rankFromNeighbors + danglingContribution)
		        + teleportationProbability / getTotalNumVertices();
	}
	
	/**
	 * Compute method.
	 */
	@Override
	public void compute(
	        Vertex<IntWritable, DoubleWritable, NullWritable> vertex,
	        Iterable<DoubleWritable> messages) throws IOException {
		double stateProbability;
		
		if (getSuperstep() > 0) {
			
			double previousStateProbability = vertex.getValue().get();
			stateProbability = recompute(vertex, messages,
			        TELEPORTATION_PROBABILITY);
			
			// Important: rescale for numerical stability
			stateProbability /= getPreviousCumulativeProbability();
			
			doubleWritable.set(Math.abs(stateProbability
			        - previousStateProbability));
			aggregate(L1_NORM_OF_PROBABILITY_DIFFERENCE, doubleWritable);
			
		} else {
			stateProbability = initialProbability();
		}
		
		vertex.getValue().set(stateProbability);
		
		aggregate(CUMULATIVE_PROBABILITY, vertex.getValue());
		
		// Compute dangling node contribution for next superstep
		if (vertex.getNumEdges() == 0) {
			aggregate(NUM_DANGLING_VERTICES, one);
			aggregate(CUMULATIVE_DANGLING_PROBABILITY, vertex.getValue());
		}
		
		if (getSuperstep() < MAX_SUPERSTEPS) {
			for (Edge<IntWritable, NullWritable> edge : vertex.getEdges()) {
				double transitionProbability = transitionProbability(
					        vertex, stateProbability, edge);
				doubleWritable.set(transitionProbability);
				sendMessage(edge.getTargetVertexId(), doubleWritable);
			}
		} else {
			vertex.voteToHalt();
		}
	}
}