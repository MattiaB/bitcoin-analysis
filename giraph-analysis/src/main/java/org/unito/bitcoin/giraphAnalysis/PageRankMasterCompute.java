/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * Created from RandomWalkVertexMasterCompute from 
 * giraph-examples (HEAD 065d718d79d4ed6bab0b6a1f4e025cab73bf8a30)
 * Nicolò Bidotti, 06/2014
 */

package org.unito.bitcoin.giraphAnalysis;

import org.apache.giraph.aggregators.DoubleSumAggregator;
import org.apache.giraph.aggregators.IntSumAggregator;
import org.apache.giraph.combiner.MessageCombiner;
import org.apache.giraph.master.DefaultMasterCompute;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.log4j.Logger;

/**
 * Master compute associated with {@link RandomWalkComputation}. It handles
 * dangling nodes.
 */
public class PageRankMasterCompute extends DefaultMasterCompute {
	
	/** threshold for the L1 norm of the state vector difference */
	static final double CONVERGENCE_THRESHOLD = 0.00001;
	
	/** logger */
	private static final Logger LOG = Logger
	        .getLogger(PageRankMasterCompute.class);
	
	@Override
	public void compute() {
		double danglingContribution = this.<DoubleWritable> getAggregatedValue(
		        PageRankComputation.CUMULATIVE_DANGLING_PROBABILITY).get();
		double cumulativeProbability = this
		        .<DoubleWritable> getAggregatedValue(
		                PageRankComputation.CUMULATIVE_PROBABILITY).get();
		double l1NormOfStateDiff = this.<DoubleWritable> getAggregatedValue(
		        PageRankComputation.L1_NORM_OF_PROBABILITY_DIFFERENCE).get();
		long numDanglingVertices = this.<IntWritable> getAggregatedValue(
		        PageRankComputation.NUM_DANGLING_VERTICES).get();

		LOG.info("[Superstep " + getSuperstep() + "] Dangling contribution = "
		        + danglingContribution + ", number of dangling vertices = "
		        + numDanglingVertices + ", cumulative probability = "
		        + cumulativeProbability
		        + ", L1 Norm of state vector difference = " + l1NormOfStateDiff);
		
		// Convergence check: halt once the L1 norm of the difference between
		// the state vectors fall below the threshold
		setMessageCombiner(DoubleIntSumMessageCombiner.class);
        if (getSuperstep() > 1 && l1NormOfStateDiff < CONVERGENCE_THRESHOLD) {
			haltComputation();
		}
	}
	
	@Override
	public void initialize() throws InstantiationException,
	        IllegalAccessException {
		registerAggregator(PageRankComputation.NUM_DANGLING_VERTICES,
		        IntSumAggregator.class);
		registerAggregator(PageRankComputation.CUMULATIVE_DANGLING_PROBABILITY,
		        DoubleSumAggregator.class);
		registerAggregator(PageRankComputation.CUMULATIVE_PROBABILITY,
		        DoubleSumAggregator.class);
		registerAggregator(
		        PageRankComputation.L1_NORM_OF_PROBABILITY_DIFFERENCE,
		        DoubleSumAggregator.class);
	}
    public static class DoubleIntSumMessageCombiner
            extends
            MessageCombiner<IntWritable, DoubleWritable> {
        @Override
        public void combine(IntWritable vertexIndex, DoubleWritable originalMessage,
                            DoubleWritable messageToCombine) {
            originalMessage.set(originalMessage.get() + messageToCombine.get());
        }

        @Override
        public DoubleWritable createInitialMessage() {
            return new DoubleWritable(0);
        }
    }
}