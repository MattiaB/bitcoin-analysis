/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.unito.bitcoin.giraphAnalysis.graphs;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

public class VertexDegrees implements Writable {
	private int vertexInDegree;
	private int vertexOutDegree;
	private int inNeighbourhoodInDegree;
	private int inNeighbourhoodOutDegree;
	private int outNeighbourhoodInDegree;
	private int outNeighbourhoodOutDegree;
	
	public VertexDegrees() {
		this.vertexInDegree = 0;
		this.vertexInDegree = 0;
		this.inNeighbourhoodInDegree = 0;
		this.inNeighbourhoodOutDegree = 0;
		this.outNeighbourhoodInDegree = 0;
		this.outNeighbourhoodOutDegree = 0;
	}
	
	public VertexDegrees(int vertexInDegree, int vertexOutDegree,
            int inNeighbourhoodInDegree, int inNeighbourhoodOutDegree,
            int outNeighbourhoodInDegree, int outNeighbourhoodOutDegree) {
	    this.vertexInDegree = vertexInDegree;
	    this.vertexOutDegree = vertexOutDegree;
	    this.inNeighbourhoodInDegree = inNeighbourhoodInDegree;
	    this.inNeighbourhoodOutDegree = inNeighbourhoodOutDegree;
	    this.outNeighbourhoodInDegree = outNeighbourhoodInDegree;
	    this.outNeighbourhoodOutDegree = outNeighbourhoodOutDegree;
    }

	public int getVertexInDegree() {
		return vertexInDegree;
	}

	public void setVertexInDegree(int vertexInDegree) {
		this.vertexInDegree = vertexInDegree;
	}

	public int getVertexOutDegree() {
		return vertexOutDegree;
	}

	public void setVertexOutDegree(int vertexOutDegree) {
		this.vertexOutDegree = vertexOutDegree;
	}
	
	public int getInNeighbourhoodInDegree() {
		return inNeighbourhoodInDegree;
	}

	public void setInNeighbourhoodInDegree(int inNeighbourhoodInDegree) {
		this.inNeighbourhoodInDegree = inNeighbourhoodInDegree;
	}

	public int getInNeighbourhoodOutDegree() {
		return inNeighbourhoodOutDegree;
	}

	public void setInNeighbourhoodOutDegree(int inNeighbourhoodOutDegree) {
		this.inNeighbourhoodOutDegree = inNeighbourhoodOutDegree;
	}

	public int getOutNeighbourhoodInDegree() {
		return outNeighbourhoodInDegree;
	}

	public void setOutNeighbourhoodInDegree(int outNeighbourhoodInDegree) {
		this.outNeighbourhoodInDegree = outNeighbourhoodInDegree;
	}

	public int getOutNeighbourhoodOutDegree() {
		return outNeighbourhoodOutDegree;
	}

	public void setOutNeighbourhoodOutDegree(int outNeighbourhoodOutDegree) {
		this.outNeighbourhoodOutDegree = outNeighbourhoodOutDegree;
	}
	
	@Override
    public boolean equals(Object obj) {
	    if (this == obj)
		    return true;
	    if (obj == null)
		    return false;
	    if (getClass() != obj.getClass())
		    return false;
	    VertexDegrees other = (VertexDegrees) obj;
	    if (inNeighbourhoodInDegree != other.inNeighbourhoodInDegree)
		    return false;
	    if (inNeighbourhoodOutDegree != other.inNeighbourhoodOutDegree)
		    return false;
	    if (outNeighbourhoodInDegree != other.outNeighbourhoodInDegree)
		    return false;
	    if (outNeighbourhoodOutDegree != other.outNeighbourhoodOutDegree)
		    return false;
	    if (vertexInDegree != other.vertexInDegree)
		    return false;
	    if (vertexOutDegree != other.vertexOutDegree)
		    return false;
	    return true;
    }

	@Override
	public void readFields(DataInput in) throws IOException {
		this.vertexInDegree = in.readInt();
		this.vertexOutDegree = in.readInt();
		this.inNeighbourhoodInDegree = in.readInt();
		this.inNeighbourhoodOutDegree = in.readInt();
		this.outNeighbourhoodInDegree = in.readInt();
		this.outNeighbourhoodOutDegree = in.readInt();
	}
	
	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(vertexInDegree);
		out.writeInt(vertexOutDegree);
		out.writeInt(inNeighbourhoodInDegree);
		out.writeInt(inNeighbourhoodOutDegree);
		out.writeInt(outNeighbourhoodInDegree);
		out.writeInt(outNeighbourhoodOutDegree);
	}
}