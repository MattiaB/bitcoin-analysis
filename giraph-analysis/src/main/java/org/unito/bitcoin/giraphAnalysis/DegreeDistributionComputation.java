/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.unito.bitcoin.giraphAnalysis;

import org.apache.giraph.combiner.MessageCombiner;
import org.apache.giraph.combiner.SimpleSumMessageCombiner;
import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.graph.Vertex;
import org.apache.giraph.master.DefaultMasterCompute;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.log4j.Logger;
import org.unito.bitcoin.giraphAnalysis.graphs.VertexDegrees;

import java.io.IOException;

/**
 * Simple computation to calculate the in/out-degrees of each vertex and the
 * in/out-degrees of its in/out-neighbourhoods.
 */
public class DegreeDistributionComputation
        extends DefaultMasterCompute {
    private static final Logger LOG = Logger
	        .getLogger(DegreeDistributionComputation.class);
    
    /**
     * Executes the operations necessary to calculate in/out-degrees; the
     * algorithm is as follows:
     *  superstep 0:
     * 		send ONE to all out-edges
     * superstep 1:
     * 		receive messages from our in-neighbourhood
     * 		calculate own in/out-degrees
     * 		send our out-degrees to our out-neighbourhood
     * superstep 2:
     * 	    receive messages from our in-neighbourhood
     * 	    calculate own in-neighbourhood-OUT-degrees
     *      send our in-degrees to our out-neighbourhood
     * superstep 3:
     * 	    receive messages from our in-neighbourhood
     * 	    calculate own in-neighbourhood-IN-degrees
     *      send our ID to all out-edges
     * superstep 4:
     *      send our out-degrees to all in-neighbourhood
     * superstep 5:
     *      receive messages from our out-neighbourhood
     *      calculate own out-neighbourhood-OUT-degrees
     *      send our ID to all out-edges
     * superstep 6:
     *      send our in-degrees to all in-neighbourhood
     * superstep 7:
     *      receive messages from our out-neighbourhood
     *      calculate own out-neighbourhood-IN-degrees
     */
    public static class DegreeDistributionInNeighbourhood extends BasicComputation<IntWritable,
            VertexDegrees, NullWritable, IntWritable> {
        private static final Logger LOG = Logger
                .getLogger(DegreeDistributionInNeighbourhood.class);

        @Override
        public void compute(Vertex<IntWritable, VertexDegrees, NullWritable> vertex, Iterable<IntWritable> messages) throws IOException {
            VertexDegrees degrees = vertex.getValue();
            switch ((int) getSuperstep()) {
                case 1: {
                    //calculate our in/out-degrees
                    int inDegree, outDegree;
                    outDegree = vertex.getNumEdges();
                    inDegree = sumAllMessages(messages);


                    degrees.setVertexInDegree(inDegree);
                    degrees.setVertexOutDegree(outDegree);
                    sendMessageToAllEdges(vertex, new IntWritable(outDegree));

                    break;
                }
                case 2: {
                    //receive messages from our in-neighbourhood; calculate its out-degrees
                    int inNeighbourhoodOutDegree = sumAllMessages(messages);
                    degrees.setInNeighbourhoodOutDegree(inNeighbourhoodOutDegree);
                    sendMessageToAllEdges(vertex, new IntWritable(degrees.getVertexInDegree()));

                    break;
                }
                case 3: {
                    //receive messages from our in-neighbourhood; calculate its in-degrees
                    int inNeighbourhoodInDegree = sumAllMessages(messages);
                    degrees.setInNeighbourhoodInDegree(inNeighbourhoodInDegree);
                    sendMessageToAllEdges(vertex, vertex.getId());

                    break;
                }
            }
        }
    }

    public static class DegreeDistributionOutNeighbourhood extends BasicComputation<IntWritable,
            VertexDegrees, NullWritable, IntWritable> {

        @Override
        public void compute(Vertex<IntWritable, VertexDegrees, NullWritable> vertex, Iterable<IntWritable> messages) throws IOException {
            VertexDegrees degrees = vertex.getValue();
            switch ((int) getSuperstep()) {
                case 4: {
                    //
                    int outDegree = degrees.getVertexOutDegree();

                    sendMessageToMultipleEdges(
                            messages.iterator(),
                            new IntWritable(outDegree)
                    );

                    break;
                }
                case 5: {
                    //receive messages from our out-neighbourhood; calculate its out-degrees
                    int outNeighbourhoodOutDegree = sumAllMessages(messages);
                    degrees.setOutNeighbourhoodOutDegree(outNeighbourhoodOutDegree);
                    sendMessageToAllEdges(vertex, vertex.getId());
                    break;
                }
                case 6: {
                    //
                    int inDegree = degrees.getVertexInDegree();
                    sendMessageToMultipleEdges(
                            messages.iterator(),
                            new IntWritable(inDegree)
                    );

                    break;
                }
                case 7: {
                    //receive messages from our out-neighbourhood; calculate its out-degrees
                    int outNeighbourhoodInDegree = sumAllMessages(messages);
                    degrees.setOutNeighbourhoodInDegree(outNeighbourhoodInDegree);
                    vertex.voteToHalt();

                    break;
                }
            }
        }
    }

    private static int sumAllMessages(Iterable<IntWritable> messages) {
        int sum = 0;
        for (IntWritable message : messages) {
            sum += message.get();
        }
        return sum;
    }

    public static class SendOne extends BasicComputation<IntWritable,
            VertexDegrees, NullWritable, IntWritable> {
        private static final IntWritable ONE = new IntWritable(1);

        @Override
        public void compute(Vertex<IntWritable, VertexDegrees, NullWritable> vertex, Iterable<IntWritable> messages) throws IOException {
            sendMessageToAllEdges(vertex, ONE);
        }
    }
    public static class SimpleIntSumMessageCombiner
            extends MessageCombiner<IntWritable, IntWritable> {

        @Override
        public void combine(IntWritable vertexIndex, IntWritable originalMessage,
                            IntWritable messageToCombine) {
            originalMessage.set(originalMessage.get() + messageToCombine.get());
        }

        @Override
        public IntWritable createInitialMessage() {
            return new IntWritable(0);
        }
    }


    @Override
    public final void compute() {
        long superStep = getSuperstep();
        if (superStep == 0) {
            setComputation(SendOne.class);
            setMessageCombiner(SimpleIntSumMessageCombiner.class);
        }
        if (superStep == 1) {
            setComputation(DegreeDistributionInNeighbourhood.class);
        }
        if (superStep == 4) {
            setComputation(DegreeDistributionOutNeighbourhood.class);
        }
        if (superStep >= 3){
            if (superStep % 2 == 1) {
                setMessageCombiner(null);
            } else {
                setMessageCombiner(SimpleIntSumMessageCombiner.class);
            }
        }
        if (superStep > 8) {
            throw new IllegalStateException("Computation is still running after 7 supersteps!");
        }
        String computationClass = getComputation().getSimpleName();
        String messageCombinerClass = (getMessageCombiner() != null ? getMessageCombiner().getSimpleName() : "None");
        LOG.info("[Superstep " + superStep + "] Computation for this superstep: "
        		+  computationClass + ". Message combiner for this superstep: "
        		+  messageCombinerClass);
        
    }
}