/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.unito.bitcoin.giraphAnalysis.formats.hive.output;

import com.facebook.hiveio.common.HiveType;
import com.facebook.hiveio.output.HiveOutputDescription;
import com.facebook.hiveio.record.HiveWritableRecord;
import com.facebook.hiveio.schema.HiveTableSchema;
import com.google.common.base.Preconditions;
import org.apache.giraph.graph.Vertex;
import org.apache.giraph.hive.output.SimpleVertexToHive;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.unito.bitcoin.giraphAnalysis.graphs.VertexDegrees;


public class HiveOutputDegreeDistributionVertex extends SimpleVertexToHive<IntWritable,
        VertexDegrees, NullWritable> {
    @Override
    public void checkOutput(HiveOutputDescription outputDesc,
                            HiveTableSchema schema, HiveWritableRecord emptyRecord) {
        Preconditions.checkArgument(schema.columnType(0) == HiveType.INT);
        Preconditions.checkArgument(schema.columnType(1) == HiveType.INT);
        Preconditions.checkArgument(schema.columnType(2) == HiveType.INT);
        Preconditions.checkArgument(schema.columnType(3) == HiveType.INT);
        Preconditions.checkArgument(schema.columnType(4) == HiveType.INT);
        Preconditions.checkArgument(schema.columnType(5) == HiveType.INT);
        Preconditions.checkArgument(schema.columnType(6) == HiveType.INT);
    }

    @Override
    public void fillRecord(
            Vertex<IntWritable, VertexDegrees, NullWritable> vertex,
            HiveWritableRecord record) {
        VertexDegrees vertexDegrees = vertex.getValue();
        record.setInt(0, vertex.getId().get());
        record.setInt(1, vertexDegrees.getVertexInDegree());
        record.setInt(2, vertexDegrees.getVertexOutDegree());
        record.setInt(3, vertexDegrees.getInNeighbourhoodInDegree());
        record.setInt(4, vertexDegrees.getInNeighbourhoodOutDegree());
        record.setInt(5, vertexDegrees.getOutNeighbourhoodInDegree());
        record.setInt(6, vertexDegrees.getOutNeighbourhoodOutDegree());
    }

}
