/*
 * Copyright Mattia Bertorello, Nicolò Bidotti 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU  Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU  Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Modified from ConnectedComponentsComputation by Nicol� Bidotti, 2014
 */

package org.unito.bitcoin.giraphAnalysis;

import org.apache.giraph.Algorithm;
import org.apache.giraph.conf.IntConfOption;
import org.apache.giraph.conf.LongConfOption;
import org.apache.giraph.edge.Edge;
import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.log4j.Logger;

import java.io.IOException;

/**
 * Implementation of the HCC algorithm that identifies connected components and
 * assigns each vertex its "component identifier" (the smallest vertex id
 * in the component)
 * <p>
 * The idea behind the algorithm is very simple: propagate the smallest
 * vertex id along the edges to all vertices of a connected component. The
 * number of supersteps necessary is equal to the length of the maximum
 * diameter of all components + 1
 * <p>
 * The original Hadoop-based variant of this algorithm was proposed by Kang,
 * Charalampos, Tsourakakis and Faloutsos in
 * "PEGASUS: Mining Peta-Scale Graphs", 2010
 * <p>
 * http://www.cs.cmu.edu/~ukang/papers/PegasusKAIS.pdf
 */
@Algorithm(
        name = "Entity discovery",
        description = "Discovers entities in the graph (\"connected components\" inferred from the structure of the graph)"
)
public class EntityDiscoveryComputation extends
        BasicComputation<IntWritable, IntWritable, NullWritable, IntWritable> {
    private static final Logger LOG = Logger.getLogger(EntityDiscoveryComputation.class);
    /** The shortest paths id */
    public static final IntConfOption MESSAGE_LIMIT =
            new IntConfOption("bitcoinAnalysis.MessageLimit", 2,
                    "The % of message send at every super step");

    /**
     * Propagates the smallest vertex id to all neighbors. Will always choose to
     * halt and only reactivate if a smaller id has been sent to it.
     *
     * @param vertex   Vertex
     * @param messages Iterator of messages from the previous superstep.
     * @throws IOException
     */
    @Override
    public void compute(
            Vertex<IntWritable, IntWritable, NullWritable> vertex,
            Iterable<IntWritable> messages) throws IOException {
        LOG.debug("starting compute on vertex: " + vertex.getId());

        final int limit = MESSAGE_LIMIT.get(getConf());
        final long limitWithSuperstep = getSuperstep() % limit;

        int currentComponent = vertex.getValue().get();
        boolean changed = limitWithSuperstep != 0;

        // First superstep is special, because we can simply look at the neighbors
        if (getSuperstep() == 0) {
            currentComponent = vertex.getId().get();
            for (Edge<IntWritable, NullWritable> edge : vertex.getEdges()) {
                int neighbor = edge.getTargetVertexId().get();
                if (neighbor < currentComponent) {
                    currentComponent = neighbor;
                    changed = true;
                }
            }
        }

        for (IntWritable message : messages) {
            int candidateComponent = message.get();
            if (candidateComponent < currentComponent) {
                currentComponent = candidateComponent;
                changed = true;
            }
        }

        // Only need to send value if it is not the own id
        // propagate new component id to the neighbors
        if (changed) {
            vertex.setValue(new IntWritable(currentComponent));
            int numMessage = 0;
            for (Edge<IntWritable, NullWritable> edge : vertex.getEdges()) {
                if ((numMessage % (limit - limitWithSuperstep)) == 0 && edge.getTargetVertexId().get() > currentComponent) {
                    IntWritable neighbor = edge.getTargetVertexId();
                    sendMessage(neighbor, vertex.getValue());
                }
                numMessage += 1;
            }
            if ((getSuperstep() + 1) % limit == 0) {
                vertex.voteToHalt();
            }
        } else {
            vertex.voteToHalt();
        }
    }
}