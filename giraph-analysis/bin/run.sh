#!/bin/sh
# Copyright Mattia Bertorello, Nicolò Bidotti 2014
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU  Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU  Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Entity Discovery Computation
hive --service jar giraph-analysis-0.1-SNAPSHOT-job.jar org.apache.giraph.hive.HiveGiraphRunner \
	--computationClass org.unito.bitcoin.giraphAnalysis.EntityDiscoveryComputation \
	-ei "org.apache.giraph.hive.input.edge.examples.HiveIntNullEdge, entities_edges" \
	-o "org.unito.bitcoin.giraphAnalysis.formats.hive.output.HiveOutputIntIntVertex, entities" \
	-hiveconf "mapred.job.tracker=hadoop-master" -hiveconf "giraph.zkList=localhost:2181" \
	-w 1 -hiveconf "giraph.SplitMasterWorker=false"

# Degree Discovery Computation
hive --service jar giraph-analysis-0.1-SNAPSHOT-job.jar org.apache.giraph.hive.HiveGiraphRunner \
	-hiveconf "giraph.masterComputeClass=org.unito.bitcoin.giraphAnalysis.DegreeDistributionComputation" \
	--computationClass org.unito.bitcoin.giraphAnalysis.DegreeDistributionComputation\$SendOne \
	-ei "org.apache.giraph.hive.input.edge.examples.HiveIntNullEdge, complete_graph" \
	-o "org.unito.bitcoin.giraphAnalysis.formats.hive.output.HiveOutputDegreeDistributionVertex, entities_degree" \
	-hiveconf "mapred.job.tracker=hadoop-master" -hiveconf "giraph.zkList=localhost:2181" \
	-w 1 -hiveconf "giraph.SplitMasterWorker=false"

# Page Rank Computation
hive --service jar giraph-analysis-0.1-SNAPSHOT-job.jar org.apache.giraph.hive.HiveGiraphRunner \
	-hiveconf "giraph.masterComputeClass=org.unito.bitcoin.giraphAnalysis.PageRankMasterCompute" \
	--computationClass org.unito.bitcoin.giraphAnalysis.PageRankComputation \
	-ei "org.apache.giraph.hive.input.edge.examples.HiveIntNullEdge, complete_graph" \
	-o "org.unito.bitcoin.giraphAnalysis.formats.hive.output.HiveOutputIntDoubleVertex, entities_page_rank" \
	-hiveconf "mapred.job.tracker=hadoop-master" -hiveconf "giraph.zkList=localhost:2181" \
	-w 1 -hiveconf "giraph.SplitMasterWorker=false"

# Clustering Coefficient Computation
hive --service jar giraph-analysis-0.1-SNAPSHOT-job.jar org.apache.giraph.hive.HiveGiraphRunner \
	-hiveconf "giraph.masterComputeClass=org.unito.bitcoin.giraphAnalysis.ClusteringCoefficient\$MasterCompute" \
	--computationClass org.unito.bitcoin.giraphAnalysis.ClusteringCoefficient\$SendFriendsList \
	-ei "org.apache.giraph.hive.input.edge.examples.HiveIntNullEdge, complete_graph" \
	-o "org.unito.bitcoin.giraphAnalysis.formats.hive.output.HiveOutputIntDoubleVertex, entities_clustering_coefficient" \
	-hiveconf "mapred.job.tracker=hadoop-master" -hiveconf "giraph.zkList=localhost:2181" \
	-w 1 -hiveconf "giraph.SplitMasterWorker=false"
