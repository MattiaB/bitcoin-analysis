# Progetto di Reti Complesse, a.a. 2013/2014
*Analisi del grafo delle transazioni della blockchain di Bitcoin.*

*Mattia Bertorello, Nicolò Bidotti*

## Introduzione
Lo scopo del progetto è di effettuare un'analisi del grafo delle transazioni 
contenute nella blockchain di Bitcoin, una moneta virtuale appartenente alle 
cosiddette cryptocurrencies (criptomonete). Il concetto centrale di Bitcoin è 
l'uso della cosiddetta blockchain, una catena di blocchi che contiene tutte le
transazioni effettuate tra i suoi utilizzatori. Si tratta in sostanza di un 
database distribuito tra i nodi della rete peer-to-peer usato per garantire la
correttezza e la validità delle transazioni e tenerne traccia senza usare 
un'autorità centrale. 

Per ulteriori dettagli sul funzionamento di Bitcoin, si possono trovare numerosi
 link sul [sito ufficiale](http://www.bitcoin.org) e su 
 [Wikipedia](http://en.wikipedia.org/wiki/Bitcoin). 


## Organizzazione del progetto
Il progetto è gestito con [Apache Maven](http://maven.apache.org/).

Il codice è suddiviso in diversi moduli:

* blockchain-importer: importa i dati della block chain in 3 file Parquet blocchi, transazioni di input, transazioni di output
* giraph-analysis: algoritmi per l'elaborazione e la trasformazione del grafo e 
l'esecuzione delle analisi
* scripts Pig: permettono la trasfomazione dei dati
* queries: eseguono analisi sui dati

## Software utilizzato
Tutto il software utilizzato è open-source.

### [Apache Hadoop](http://hadoop.apache.org/)
Per supportare il processing distribuito del grafo, si è scelto di utilizzare la
 piattaforma Hadoop.
 
Versione 2.3.0

### [Apache Hive](https://hive.apache.org/)
Versione 0.12
### [Cloudera Impala](http://impala.io/)
Versione 1.4
### [Apache Pig](http://pig.apache.org/)
Versione 0.12
### [Apache Parquet](http://parquet.incubator.apache.org/)
Versione 1.2.5

### [Apache Giraph](http://giraph.apache.org/)
La parte di analisi viene svolta facendo uso di Apache Giraph.

Versione f6845a3 custom


### [Okapi](http://grafos.ml/#Okapi)
Per alcune analisi viene utilizzato codice della libreria Okapi, versione 0.3.5
per MapReduce2.

Versione bb94586

### [BitcoinJ](https://bitcoinj.github.io/)
Versione 0.12



## Bibliografia

[Bitcoin: A Peer-to-Peer Electronic Cash System - Satoshi Nakamoto](https://bitcoin.org/bitcoin.pdf)

[Quantitative Analysis of the Full Bitcoin Transaction Graph - Dorit Ron, Adi Shamir](https://eprint.iacr.org/2012/584.pdf)

[An Analysis of Anonymity in the Bitcoin System - Fergal Reid, Martin Harrigan](http://arxiv.org/pdf/1107.4524v2.pdf)

[Bitcoin Transaction Graph Analysis - Michael Fleder, Michael S. Kester, Sudeep Pillai](http://people.csail.mit.edu/spillai/data/papers/bitcoin-transaction-graph-analysis.pdf)

[Structure and Anonymity of the Bitcoin Transaction Graph - Micha Ober , Stefan Katzenbeisser, Kay Hamacher](http://www.mdpi.com/1999-5903/5/2/237)

[A Fistful of Bitcoins - Characterizing Payments Among Men with No Names - Sarah Meiklejohn, Marjori Pomarole, Grant Jordan, Kirill Levchenko, Damon McCoy, Geoffrey M. Voelker, Stefan Savage](http://cseweb.ucsd.edu/~smeiklejohn/files/imc13.pdf)
